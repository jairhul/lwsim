cmake_minimum_required (VERSION 3.2)
project (lwsim)

set(LWSIM_MAJOR_VERSION 1)
set(LWSIM_MINOR_VERSION 0)
set(LWSIM_VERSION ${LWSIM_MAJOR_VERSION}.${LWSIM_MINOR_VERSION})

message(STATUS "Configuring LWSIM ${LWSIM_VERSION}")

# cmake settings
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_INCLUDE_CURRENT_DIR_IN_INTERFACE ON)
include(GenerateExportHeader)
set(CMAKE_MACOSX_RPATH ON) # set MACOSX_RPATH explicitly (CMP0042)

# add source dir as a place for CMake modules
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules)

# Check if compiler is compatible
include(cmake/CompilerCheck.cmake)

# GSL
find_package(GSL)
# CLHEP - for future
#include(cmake/CLHEP.cmake)
# ROOT - for minuit - for future
include(cmake/ROOT.cmake)

# add a target to generate API documentation with Doxygen
include (cmake/Doxygen.cmake)

# headers
include_directories(include)
include_directories(${GSL_INCLUDE_DIRS})

# sources
file(GLOB SOURCES "src/*.cc")

# rpath for libraries to work from installation correctly
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

# shared library
set(LWSIM_LIB_NAME "lwsimLib")
add_library(${LWSIM_LIB_NAME} SHARED ${SOURCES})
#target_link_libraries(${LWSIM_LIB_NAME} ${CLHEP_LIBRARIES}) # for future
target_link_libraries(${LWSIM_LIB_NAME} ${GSL_LIBRARIES})
target_link_libraries(${LWSIM_LIB_NAME} ${ROOT_LIBRARIES} Minuit2)
generate_export_header(${LWSIM_LIB_NAME})
set_property(TARGET ${LWSIM_LIB_NAME} PROPERTY VERSION ${LWSIM_VERSION})

# static library
add_library(${LWSIM_LIB_NAME}Static STATIC ${SOURCES})

# executable tests
add_subdirectory(test)

# installation and packaging
install(TARGETS ${LWSIM_LIB_NAME} ${LWSIM_LIB_NAME}Static EXPORT LwsimTargets
	INCLUDES DESTINATION include
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib)

install(DIRECTORY include/ DESTINATION include/lwsim
        FILES_MATCHING PATTERN "*.hh")

set(INCLUDE_INSTALL_DIR include/ CACHE PATH "include")
set(LIB_INSTALL_DIR lib/ CACHE PATH "lib")
set(SYSCONFIG_INSTALL_DIR lib/../ CACHE PATH "sysconfig")

export(EXPORT LwsimTargets
  FILE "${CMAKE_CURRENT_BINARY_DIR}/Lwsim/LwsimTargets.cmake"
  NAMESPACE Lwsim::
)

include(CMakePackageConfigHelpers)
configure_package_config_file(cmake/LwsimConfig.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/Lwsim/LwsimConfig.cmake
			      INSTALL_DESTINATION ${LIB_INSTALL_DIR}/Lwsim/cmake
			      PATH_VARS INCLUDE_INSTALL_DIR SYSCONFIG_INSTALL_DIR)
write_basic_package_version_file(${CMAKE_CURRENT_BINARY_DIR}/Lwsim/LwsimConfigVersion.cmake
  VERSION ${LWSIM_VERSION}
  COMPATIBILITY AnyNewerVersion
)


#configure_file(cmake/LwsimConfig.cmake
#  "${CMAKE_CURRENT_BINARY_DIR}/Lwsim/LwsimConfig.cmake"
#)

set(ConfigPackageLocation lib/cmake/Lwsim)
install(EXPORT LwsimTargets
  FILE
    LwsimTargets.cmake
  NAMESPACE
    Lwsim::
  DESTINATION
    ${ConfigPackageLocation}
)
install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/Lwsim/LwsimConfig.cmake
    "${CMAKE_CURRENT_BINARY_DIR}/Lwsim/LwsimConfigVersion.cmake"
  DESTINATION
    ${ConfigPackageLocation}
  COMPONENT
    Devel
)
