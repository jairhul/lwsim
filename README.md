# README #

### Laserwire Simulation ###

* Simulate a laserwire scanner and beam profile scans.
* Estimate the number of Compton scattered photons produced.
* Simluate 1D and 2D laserwire scans.
* Numerical overlap integral for divergent laser beams.
* Gaussian overlap for simple laserwire systems and comparison.

### Authors ###

* Laurie Nevay
* Siobhan Alden

### Licence ###

* GPL3 - see included LICENCE.txt

Provided as is.

Any research based on this must cite:
L. Nevay et al., Phys. Rev. ST Accel. Beams 17, 072801 (2014).
https://journals.aps.org/prab/abstract/10.1103/PhysRevSTAB.17.072801

### Requirements ###

* CMake >= 3.2
* GSL
* C++11 compatible compiler
* ROOT6

### Copyright ###

Copyright Royal Holloway, University of London 2017.

### Description ###

* LaserGaussian class to describe a laser focus.
* Laserwire.hh provides functions to calculate the overlap integrals and the photon flux.
* LaserwireScan.hh provides data sets for given parameters.
* Fifo.hh provides file writing routines.

### Examples ###
* The test directory contains example executables for various applications.
* These demonstrate each feature of the library.
* Python plotting scripts are also provided that use Matplotlib.
* Example plots from the test executables and accompanying Python scripts are in test/exampleResults

### Build and Installation ###

* Uses CMake out of build system.


		mkdir lwsim-build
		mkdir lwsim-install
		cd lwsim-build
		cmake ../lwsim
		make
		make install
	

### External Programs ###

* A CMake configuration file is provided for easy use of library with your own programs.
* Look at externalexample/CMakeLists.txt for a simple example.

