if (DEFINED ENV{ROOTSYS})
  message(STATUS "Use ROOTSYS from environment: $ENV{ROOTSYS}")
  set (ROOTSYS $ENV{ROOTSYS})
endif()

# find ROOT of at least version 6
find_package(ROOT 6.0 REQUIRED)

# ROOT doesn't implement the version and subversion number in CMAKE as it should, so
# the above find package doesn't match the version required. Need to decode version ourselves
if (ROOT_VERSION VERSION_LESS "6.00")
  message(FATAL_ERROR "ROOT Version 6 required. Version ${ROOT_MAJOR_VERSION} found")
endif()

# add ROOT include directory
include_directories(${ROOT_INCLUDE_DIR})
