#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "LaserwireScan.hh"

#include <map>
#include <vector>

int main()
{
  LWS::Laser* l = new LWS::LaserGaussian(532e-9, 2.3e-6, 1.2);

  std::vector<double> xPositions = LWS::GenerateXData(-1.5e-3, 1.5e-3, 500);

  double sigma_ex = 100e-6; // um
  double sigma_ey = 1e-6;

  std::vector<int> sigmas = {0,1,2,3,5,10,30};
  
  for (const auto s : sigmas)
    {
      auto result = LWS::HorizontalScanOI(l, sigma_ex, sigma_ey, xPositions, (double)s * sigma_ey);
      std::string name = "horizontalOIScan" + std::to_string(s) + ".txt";
      LWS::WriteScanToFile(name, xPositions, "X", result, "OverlapIntegral");
    }

  delete l;
  
  return 0;
}
