#ifndef LWSIM_CONSTANTS_H
#define LWSIM_CONSTANTS_H

#include <cmath>

// for doxygen
/** @file */

namespace LWS
{
  /// Pi
  const double pi = 3.14159265358979323846;

  /// Two pi
  const double twopi = 2. * pi;
  
  /// Speed of light in m s^-1.
  const double c_light = 299792458;

  /// Speed of light squared in m^2 s^-2.
  const double c_light_squared = c_light * c_light;

  /// Vacuum permittivity in  F m^−1. 
  const double epsilon0 = 8.854187817e-12;

  /// Electron mass in kg.
  const double electron_mass = 9.10938356e-31;

  /// Electron mass in GeV.
  const double electron_mass_gev = 0.5109989461e-3;

  /// Elementary charge in C.
  const double q = 1.6021766208e-19;

  /// Planck constant
  const double h = 6.626070040e-34;

  /// Thomson scattering cross-section.
  const double thomson_cross_section = ((8. * pi) /3.) * std::pow((q*q / (4. * pi * epsilon0 * electron_mass * c_light_squared)), 2);

  /// hydrogen ion mass
  const double hydrogen_ion_mass_gev = 0.93929401082;

  /// electron charge
  const double electron_charge = 1.60217662e-19;

  /// joules to gev
  const double joules_gev = 6.242e+9;
}

#endif
