#ifndef LWSIM_FIFO_H
#define LWSIM_FIFO_H

#include "Point.hh"

#include <string>
#include <vector>

// for doxygen
/** @file */

namespace LWS
{
  /// Write two vectors of x,y data to a tab-delimited column file. The
  /// first row is always the column titles.
  void WriteScanToFile(std::string                fileName,
		       const std::vector<double>& x,
		       std::string                xColumnName,
		       const std::vector<double>& y,
		       std::string                yColumnName);

  /// Write a series of 2D sampled points to a file.
  void WriteScan2DToFile(std::string                    fileName,
			 const std::vector<LWS::point>& data);


  /// Write a simple vector column out - for development purposes.
  void WriteVector(std::string                fileName,
		   const std::vector<double>& v);
}

#endif
