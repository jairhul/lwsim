#ifndef LWSIM_FIT_H
#define LWSIM_FIT_H

#include <vector>

namespace LWS
{
  class FitParameters;
  class Laser;
  
  double WeightedMean(const std::vector<double>& x,
		      const std::vector<double>& weights);

  LWS::FitParameters GaussianInitial(const std::vector<double>& y,
				     const std::vector<double>& signal);
  
  LWS::FitParameters FitGaussian(const std::vector<double>& x,
				 const std::vector<double>& signal,
				 const std::vector<double>& signal_err,
				 bool verbose = false);
  
  LWS::FitParameters FitVerticalScanEmpirical(const Laser* laser,
					      double sigmaX,
					      const std::vector<double>& y,
					      const std::vector<double>& signal,
					      const std::vector<double>& signalError,
					      bool verbose = false);
  
  LWS::FitParameters FitHorizontalScanEmpirical(const Laser* laser,
						double sigmaY,
						const std::vector<double>& x,
						const std::vector<double>& signal,
						const std::vector<double>& signalError,
						bool verbose = false);
}

#endif
