#ifndef LWSIM_FITFUNCTION_H
#define LWSIM_FITFUNCTION_H

#include "root6/Minuit2/FCNBase.h"

#include <iostream>
#include <vector>

namespace LWS
{
  class Laser;

  std::vector<double> Gaussian(const std::vector<double> x,
                                    double amplitude,
                                    double offset,
                                    double centre,
                                    double sigmaY);

  class FcnB: public ROOT::Minuit2::FCNBase
  {
  public:
    FcnB(const std::vector<double>& pos,
	 const std::vector<double>& meas,
	 const std::vector<double>& mstd,
	 bool verboseIn = false):
      theMeasurements(meas),
      thePositions(pos),
      theMstds(mstd),
      verbose(verboseIn)
    {;}
    virtual ~FcnB(){;}

    // Minuit estimates the error by varying each parameter until the function
    // increase by value up defined here.  For Chi2 this is 1 and for negative
    // log-likelihood it is 0.5.
    virtual double Up() const {return 1.;}

    virtual void PrintVerboseHeader()
    {std::cout << "Amplitude\tOffset\tCentre\tSigma_ey" << std::endl;}

    virtual void PrintParameters(const std::vector<double>& par) const
    {
      for (auto v : par)
        {std::cout << v << "\t";}
    }
    
  protected:
    double RedChi2(const std::vector<double>& model) const;
    
    std::vector<double> theMeasurements;
    std::vector<double> thePositions;
    std::vector<double> theMstds;
    bool                verbose;
  };

  class FcnGaussian: public FcnB
  {
  public:
    using FcnB::FcnB;
    virtual ~FcnGaussian(){;}
    virtual double operator()(const std::vector<double>& par) const;
  };
  
  class FcnOIEV: public FcnB
  {
  public:
    FcnOIEV(const LWS::Laser* laserIn,
	    double sigmaXIn,
	    const std::vector<double>& pos,
	    const std::vector<double>& meas,
	    const std::vector<double>& mstd,
	    bool verboseIn = false):
      FcnB(pos, meas, mstd, verboseIn),
      laser(laserIn),
      sigmaX(sigmaXIn)
    {;}
    virtual ~FcnOIEV(){;}
    virtual double operator()(const std::vector<double>& par) const;
    
  private:
    const LWS::Laser*   laser;
    const double        sigmaX;
  };

  class FcnOIEH: public FcnB
  {
  public:
    FcnOIEH(const Laser* laserIn,
	    double sigmaYIn,
	    const std::vector<double>& pos,
	    const std::vector<double>& meas,
	    const std::vector<double>& mstd,
	    bool verboseIn = false):
      FcnB(pos, meas, mstd, verboseIn),
      laser(laserIn),
      sigmaY(sigmaYIn)
    {;}
    virtual ~FcnOIEH(){;}
    virtual double operator()(const std::vector<double>&) const;
  
  private:
    const LWS::Laser*   laser;
    const double        sigmaY;
  };
}

#endif

