#ifndef LWSIM_FITPARAMETERS_H
#define LWSIM_FITPARAMETERS_H

#include <limits>
#include <ostream>

namespace LWS
{
  class FitParameters
  {

   public:
    double centre = 0;
    double offset = 0;
    double sigma = 0;
    double amplitude = 0;
    double metric = 0;
    double centre_err = 0;
    double offset_err = 0;
    double sigma_err = 0;
    double amplitude_err = 0;

    bool   limitsSet = false;
    double limitCentreLow     = -std::numeric_limits<double>::max();
    double limitCentreHigh    =  std::numeric_limits<double>::max();
    double limitOffsetLow     = -std::numeric_limits<double>::min();
    double limitOffsetHigh    =  std::numeric_limits<double>::max();
    double limitSigmaLow      = -std::numeric_limits<double>::min();
    double limitSigmaHigh     =  std::numeric_limits<double>::max();
    double limitAmplitudeLow  = -std::numeric_limits<double>::min();
    double limitAmplitudeHigh =  std::numeric_limits<double>::max();

    friend std::ostream& operator<<(std::ostream& os, const FitParameters& fp)
    {
      os << "Centre    " << fp.centre    << " +- " << fp.centre_err    << std::endl;
      os << "Offset    " << fp.offset    << " +- " << fp.offset_err    << std::endl;
      os << "Sigma     " << fp.sigma     << " +- " << fp.sigma_err     << std::endl;
      os << "Amplitude " << fp.amplitude << " +- " << fp.amplitude_err << std::endl;
      os << "Metric    " << fp.metric    << std::endl;
      return os;
    };
  };
}

#endif
