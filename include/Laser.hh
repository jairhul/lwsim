#ifndef LWSIM_LASER_H
#define LWSIM_LASER_H

#include <cmath>

namespace LWS
{
  /**
   * @brief Base class for laser.
   * 
   * This represents a laser focus.
   * 
   * @author Laurie Nevay
   */
  class Laser
  {
  public:
    Laser(double wavelengthIn,
          double sigmaTIn      = 1e-9,
          double pulseEnergyIn = 1.0,
	  double thetaIn       = 0);
    
    virtual ~Laser(){;}

    /// Main function to access the w (from maximum to 1/e^2 - w = 2*sigma
    /// for a Gaussian beam) in the frame of the laser - ie without any rotation
    /// or mixing of Y. This by default returns the result of the classic
    /// w vs z equation for a Gaussian beam, but can be overloaded for other
    /// purposes.
    virtual double WXUnrotated(const double& z) const = 0;

    /// Main function to access the w (from maximum to 1/e^2 - w = 2*sigma
    /// for a Gaussian beam) in the frame of the laser - ie without any rotation
    /// or mixing of X. This by default returns the result of the classic
    /// w vs z equation for a Gaussian beam, but can be overloaded for other
    /// purposes.
    virtual double WYUnrotated(const double& z) const = 0;

    /// Return the possibly rotated Wx. If rotated, the maximum extent
    /// of the ellipse in x is used. Not the actual ellipse.
    double WX(const double& z) const;

    /// Return the possibly rotated Wy. If rotated, the maximum extent
    /// of the ellipse in y is used. Not the actual ellipse.
    double WY(const double& z) const;

    /// Return 0.5x WX for a given Z. Includes rotation.
    virtual double SigmaX(const double& z) const;

    /// Return 0.5x WY for a given Z. Includes rotation.
    virtual double SigmaY(const double& z) const;

    /// Return 0.5x WX for a given Z.
    virtual double SigmaXUnrotated(const double& z) const;

    /// Return 0.5x WY for a given Z.
    virtual double SigmaYUnrotated(const double& z) const;

    /// Convenience - call both SigmaX() and SigmaY() and write out
    /// to the variables passed by reference.
    virtual void SigmaXY(const double& z,
			 double&       sigmaX,
			 double&       sigmaY) const;

    /// @{ Accessor.
    inline double Wavelength()  const {return wavelength;}
    inline double SigmaT()      const {return sigmaT;}
    inline double FWHMT()       const {return 2 * std::sqrt(2*std::log((double)2)) * sigmaT;}
    inline double PulseEnergy() const {return pulseEnergy;}
    inline double PeakPower()   const {return peakPower;}
    /// @}

  protected:
    const double wavelength; ///< Wavelength of laser.

    /// Angle that major and minor axes of laser ellipse are rotated w.r.t
    /// to lab transverse axes about the laser propagation direction.
    const double theta;
    
    const double sigmaT;      ///< Pulse duration in s as defined by sigma.
    const double pulseEnergy; ///< Laser pulse energy in J.
    const double peakPower;   ///< Peak power from pulse energy / time (FWHM) in W.
    
  private:
    /// No default constructor
    Laser() = delete;

    bool rotated; ///< Whether the laser is rotated w.r.t. the lab.
  };
}

#endif
