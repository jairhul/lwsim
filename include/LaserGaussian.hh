#ifndef LWSIM_LASERGAUSSIAN_H
#define LWSIM_LASERGAUSSIAN_H

#include "Laser.hh"

namespace LWS
{
  /**
   * @brief Class that provides a Gaussian profile laser.
   * 
   * All input parameters have to be in S.I. units.
   *
   * @author Laurie Nevay
   */
  class LaserGaussian: public Laser
  {
  public:
    /// Default is a laser with 532nm laser with a symmetric focal w of 2um
    /// and an M2 of 1.
    LaserGaussian();

    /// Symmetric laser beam with a wavelength, focussed beam size w0 and an
    /// optional M2.
    LaserGaussian(double wavelengthIn,
		  double w0In,
		  double m2In          = 1.0,
		  double sigmaTIn      = 0.5e-12, // 0.5 ps
		  double pulseEnergyIn = 100e-6); // 100 uJ

    /// Full control over all input parameters. z0x,y is the offset along the
    /// focal path of the laser from 0 for the focus location in each dimension.
    /// Theta is the optional angle of the M2 system w.r.t. the lab frame.
    LaserGaussian(double wavelengthIn,
		  double w0xIn, double m2xIn,
		  double w0yIn, double m2yIn,
		  double z0xIn,
		  double z0yIn,
		  double thetaIn       = 0,
                  double sigmaTIn      = 0.5e-12, // 0.5 ps
                  double pulseEnergyIn = 100e-6); // 100 uJ

    virtual ~LaserGaussian();

    /// Main function to access the w (from maximum to 1/e^2 - w = 2*sigma
    /// for a Gaussian beam) in the frame of the laser - ie without any rotation
    /// or mixing of Y. This by default returns the result of the classic
    /// w vs z equation for a Gaussian beam, but can be overloaded for other
    /// purposes.
    virtual double WXUnrotated(const double& z) const;

    /// Main function to access the w (from maximum to 1/e^2 - w = 2*sigma
    /// for a Gaussian beam) in the frame of the laser - ie without any rotation
    /// or mixing of X. This by default returns the result of the classic
    /// w vs z equation for a Gaussian beam, but can be overloaded for other
    /// purposes.
    virtual double WYUnrotated(const double& z) const;   

    /// @{ Accessor.
    inline double W0X()        const {return w0x;}
    inline double W0Y()        const {return w0y;}
    inline double RayleighRangeX() const {return rrx;}
    inline double RayleighRangeY() const {return rry;}
    inline double M2X()            const {return m2x;}
    inline double M2Y()            const {return m2y;}
    /// @}
    
  private:
    /// @{ Raw parameters.
    double w0x, w0y;
    double m2x, m2y;
    double z0x, z0y;
    /// @}

    // Calculated parameters.
    double rrx, rry; ///< Rayleigh range.   
  };
}

#endif
