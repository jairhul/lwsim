#ifndef LWSIM_LASERWIRE_H
#define LWSIM_LASERWIRE_H

#include <gsl/gsl_integration.h>

// for doxygen
/** @file */

namespace LWS
{
  class Laser;
  
  /// Return the maximum possible Compton-scattered photon energy,
  /// i.e. the Compton edge (in GeV). electronEnergy in GeV.
  double MaximumComptonPhotonEnergy(const Laser* laser,
                                    double electronEnergy);

  /// Calculate the Lorentz gamma of an an electron for a given total
  /// energy in GeV.
  double ElectronLorentzGammaFromEnergy(const double energy);

  /// Calculate the ratio of the Compton cross-section to the classical
  /// Thomson cross-section. The electron energy argument should be in GeV.
  double ComptonThomsonRatio(const Laser* laser,
			     const double electronEnergy);
  
  /// Calculate the Compton cross-section for a given laser beam
  /// and electron beam. The electronEnergy argument is given in GeV.
  /// This assumes a photon colliding perpendicularly (at 90 degrees) to
  /// an electron beam.
  double ComptonCrossSection(const Laser* laser,
			     const double electronEnergy);

  /// Calculate the photodetachment cross-section for a given laser beam
  /// and ion beam. The ionEnergy argument is given in GeV.
  /// This assumes a photon colliding perpendicularly (at 90 degrees) to
  /// an ion beam.
  double PhotodetachmentCrossSection(const Laser* laser,
                                     const double ionEnergy);

  /// Overlap integral where the Rayleigh range is ignored and the laser
  /// is assumed to have a constant width across the electron beam and
  /// also that both the electron beam and the laser beam have Gaussian
  /// intensity profiles. This will give units similar to the OI function.
  double OIGaussian(const Laser*  laser,
		    const double& sigma_ey,
		    const double& dy);

  /// Same as above but in SI for absolute rate calculations.
  double OIGaussianSI(const Laser*  laser,
		      const double& sigma_ey,
		      const double& dy);

  /// Numerical overlap integral where the laser beam is not a constant
  /// width across the electron beam. The laser beam and electron beams
  /// are assumed to have a Gaussian profile. sigma_ex and sigma_ey are
  /// the sigmas of the electron beam in the lab x and y dimensions. dx
  /// and dy are the laser focal point offset w.r.t. the electron bunch
  /// centre in the coordinate system of the electron bunch. An optional
  /// gsl workspace may be used (if not provided, created dynamically).
  /// epsabs and epsrel are absolute and relative numerical error targets
  /// for the numerical integration.
  double OI(const Laser*  laser,
	    const double& sigma_ex,
	    const double& sigma_ey,
	    const double& dx,
	    const double& dy,
	    gsl_integration_workspace* w = nullptr,
	    double epsabs = 0,
	    double epsrel = 1e-7);

  /// Common code to calculate photon normalisation for overlap integral. This applies
  /// only in the case of a perpendicular laser to e- beam and assuming the laser pulse
  /// is sufficiently long in time compared to the electron bunch that the laser power
  /// can be considered constant for the duration of the electron bunch.
  double PhotonNormalisation(const Laser* laser,
			     const double numberOfElectrons,
			     const double electronEnergy);

  /// Common code to calculate photon normalisation for overlap integral.
  double PhotonNormalisationIons(const Laser* laser,
                                        const double numberOfIons,
                                        const double ionEnergy);

  /// Calculate the number of Compton photons for a given arrangement
  /// of laser beam and electron beam. This calls OIFromGaussian as it
  /// assumes the laser Rayleigh range is unimportant. The electron
  /// energy is in GeV, not J.
  double ComptonPhotonsGaussian(const Laser* laser,
				const double numberOfElectrons,
				const double electronEnergy,
				const double sigma_ey,
				const double dy);
  /// Calculate the number of photodetached electrons or hydrogens for
  /// a given arrangement of laser beam and electron beam. This calls
  /// OIFromGaussian as it assumes the laser Rayleigh range is
  /// unimportant. The electron energy is in GeV, not J.
  double PhotodetachmentPhotonsGaussian(const Laser* laser,
                                        const double numberOfIons,
                                        const double ionEnergy,
                                        const double sigma_ey,
                                        const double dy);
  
  /// Calculate the number of Compton photons for a given arrangement
  /// of laser beam and electron beam. This calls OI but also provides
  /// the correct normalisation. The ion energy is in GeV, not J.
  double ComptonPhotons(const Laser*  laser,
			const double  numberOfElectrons,
			const double  electronEnergy,
			const double& sigma_ex,
			const double& sigma_ey,
			const double& dx,
			const double& dy,
			gsl_integration_workspace* w = nullptr,
			double epsabs = 0,
			double epsrel = 1e-7);

  /// Calculate the number of photodetached electrons or hydrogens  for
  /// a given arrangement of laser beam and electron beam. This calls
  /// OI but also provides the correct normalisation. The ion energy is in GeV, not J.
  double PDHydrogens(const Laser*  laser,
                     const double  numberOfIons,
                     const double  ionEnergy,
                     const double& sigma_ex,
                     const double& sigma_ey,
                     const double& dx,
                     const double& dy,
                     gsl_integration_workspace* w,
                     double epsabs =0,
                     double epsrel = 1e-7);

  /// The actual function that's numerically sampled for the numerical
  /// integration.
  double OIFunction(double x, void* params);

  /// G. Blair's version of the forumula for a symmetric Gaussian
  /// Laser and and electron beam. From the paper
  /// "Simulation of Laser-wires at CLIC using BDSIM" (no journal reference).
  double NPhotonsBlair(const Laser* l,
                       double nElectrons,
                       double electronEnergy,
                       double electronSigma);
}

#endif
