#ifndef LWSIM_LASERWIRESCAN_H
#define LWSIM_LASERWIRESCAN_H

#include "Point.hh"

#include <vector>

// for doxygen
/** @file */

namespace LWS
{
  class Laser;

  /// Generate uniformly spaced data along one axes from start to stop.
  std::vector<double> GenerateXData(const double& start,
				    const double& stop,
				    const int&    nSteps);

  /// Generate non-linear spaced data along one axes about the centre with
  /// a certain number of points. The points are approximately distributed
  /// with a cubic density profile, but also with a minimum step size near
  /// the middle. This in effect gives you a small linearly spaced section
  /// in the middle and a quickly increasing step size towards the edges.
  /// This was developed empirically for the most detailed scan with the
  /// minimum number of steps. Ideally, one would distribute data points
  /// at equal spacing along the path of the function and map that to the
  /// x axis, but given the overlap integral, this is non-trivial.
  std::vector<double> GenerateNLXData(const double& centre,
				      const int&    nSteps,
				      const double& sigma_y,
				      const double  minimumStepSize = 0.3e-6);

  /// Generate data for a vertical scan. This only calculates the overlap integral.
  std::vector<double> VerticalScanOI(const Laser*               laser,
				     const double&              sigma_ex,
				     const double&              sigma_ey,
				     const std::vector<double>& dy,
				     const double               dx= 0);
  
  /// Generate data for a horizontal scan. This only calculates the overlap integral.
  std::vector<double> HorizontalScanOI(const Laser*               laser,
				       const double&              sigma_ex,
				       const double&              sigma_ey,
				       const std::vector<double>& dx,
				       const double               dy= 0);

  /// Generate data for a vertical scan. This calculates the number of Compton
  /// scattered photons.
  std::vector<double> VerticalScan(const Laser*               laser,
				   const double               numberOfElectrons,
				   const double               electronEnergy,
				   const double&              sigma_ex,
				   const double&              sigma_ey,
				   const std::vector<double>& dy,
				   const double               dx= 0);
  /// Generate data for a vertical scan. This calculates the number of photodetached
  /// electrons and hydrogens.
  std::vector<double> VerticalScanPD(const Laser*               laser,
                                          const double               numberOfIons,
                                          const double               ionEnergy,
                                          const double&              sigma_ex,
                                          const double&              sigma_ey,
                                          const std::vector<double>& dy,
                                          const double               dx);
  /// Generate data for a horizontal scan. This calculates the number of Compton
  /// scattered photons.
  std::vector<double> HorizontalScan(const Laser*               laser,
				     const double               numberOfElectrons,
				     const double               electronEnergy,
				     const double&              sigma_ex,
				     const double&              sigma_ey,
				     const std::vector<double>& dx,
				     const double               dy= 0);

  /// Generate data for a vertical scan assuming the laser Rayleigh range is ignored
  /// and both the electron and laser intensity profiles are Gaussian. It therefore,
  /// does not vary with the horizontal offset x. Returns number of photons at each
  /// position.
  std::vector<double> VerticalScanGauss(const Laser*               laser,
					const double               numberOfElectrons,
					const double               electronEnergy,
					const double               sigma_ex,
					const double               sigma_ey,
					const std::vector<double>& dy);
  /// Generate data for a vertical scan assuming the laser Rayleigh range is ignored
  /// and both the ion and laser intensity profiles are Gaussian. It therefore,
  /// does not vary with the horizontal offset x. Returns number of electrons or hydrogens
  /// at each position.
  std::vector<double> VerticalScanGaussIons(const Laser*               laser,
                                            const double               numberOfIons,
                                            const double               ionEnergy,
                                            const double               sigma_ex,
                                            const double               sigma_ey,
                                            const std::vector<double>& dy);

  /// Generate data for a vertical scan assuming the laser Rayleigh range is ignored
  /// and both the electron and laser intensity profiles are Gaussian. It therefore,
  /// does not vary with the horizontal offset x. Returns overlap integral only. 
  std::vector<double> VerticalScanOIGauss(const Laser*               laser,
					  const double               sigma_ey,
					  const std::vector<double>& dy);

  /// For each x and y position in xPositions and yPositions, evaluate the overlap
  /// integral. Data stored in simple struct called 'LWS::point'. Outer loop is y
  /// and inner loop is x.
  std::vector<LWS::point> TwoDScanOI(const Laser*               laser,
				     const double&              sigma_ex,
				     const double&              sigma_ey,
				     const std::vector<double>& xPositions,
				     const std::vector<double>& yPositions);

  /// Similar to TwoDScanOI but includes the normalisation to give the number of
  /// Compton photons produced. electronEnergy is in GeV.
  std::vector<LWS::point> TwoDScan(const Laser*               laser,
				   const double               numberOfElectrons,
				   const double               electronEnergy,
				   const double&              sigma_ex,
				   const double&              sigma_ey,
				   const std::vector<double>& xPositions,
				   const std::vector<double>& yPositions);
}

#endif
