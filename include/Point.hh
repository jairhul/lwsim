#ifndef LWSIM_POINT_H
#define LWSIM_POINT_H

// for doxygen
/** @file */

namespace LWS
{
  /* @brief A simple structure for x,y,value data.
   * 
   * @author Laurie Nevay
   */ 
  struct point
  {
    double x;
    double y;
    double value;
  };
}

#endif
