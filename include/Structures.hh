#ifndef LWSIM_STRUCTURES_H
#define LWSIM_STRUCTURES_H


// for doxygen
/** @file */
namespace LWS
{
  class Laser;

  /// Structure for gsl integration parameters.
  struct func_parameters
  {
    double dx;
    double dy;
    double sex;
    double sey;
    double m2;
    const Laser* laser;
  };
}

#endif
