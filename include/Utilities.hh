#ifndef LWSSIM_UTILITIES_H
#define LWSSIM_UTILITIES_H

// for doxygen
/** @file */

namespace LWS
{
  /// Whether a number is non-zero.
  bool IsFinite(const double& number);

  /// Calculate the Rayleigh range for a given set of parameters. Note w0 != sigma.
  double RayleighRange(const double& wavelength,
		       const double& w0,
		       const double  m2 = 1);

}

#endif
