#include "fcn.hh"

double OIEmpiricalVFcn::operator()(const std::vector<double>& par) const {

  assert(par.size() == 4);
  
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey
  
  vector<double> model = OIDataEmpiricalV (thePositions, par[0], par[1], par[2], sex, par[3], dx);

  double chi2 = 0.;
  int nmeas = theMeasurements.size();
  double nmeas_d = theMeasurements.size();
  for(int n = 0; n < nmeas; n++) {
    chi2 += ((model[n]-theMeasurements[n])/theMstds[n])*((model[n]-theMeasurements[n])/theMstds[n]);
  }
  double redchi2 = chi2 / (nmeas_d-4.0-1.0);
  #if PRINTOUT
  std::cout<<redchi2<<"\t\t"<<par[0]<<"\t"<<par[1]<<"\t"<<par[2]<<"\t"<<par[3]<<std::endl;
  #endif
  return chi2;
}

void OIEmpiricalVFcn::init()
{
  sex = ips.sex;
  dx  = ips.dx;
}

double OIEmpiricalHFcn::operator()(const std::vector<double>& par) const {

  assert(par.size() == 4);
  
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sex
  
  vector<double> model = OIDataEmpiricalH (thePositions, par[0], par[1], par[2], par[3], sey,  dy);

  double chi2 = 0.;
  int nmeas = theMeasurements.size();
  double nmeas_d = theMeasurements.size();
  for(int n = 0; n < nmeas; n++) {
    chi2 += ((model[n]-theMeasurements[n])/theMstds[n])*((model[n]-theMeasurements[n])/theMstds[n]);
  }
  double redchi2 = chi2 / (nmeas_d-4.0-1.0);
  #if PRINTOUT
  std::cout<<redchi2<<"\t\t"<<par[0]<<"\t"<<par[1]<<"\t"<<par[2]<<"\t"<<par[3]<<std::endl;
  #endif
  return chi2;
}

void OIEmpiricalHFcn::init()
{
  sey = ips.sex;
  dy  = ips.dx;
}

double OIM2VFcn::operator()(const std::vector<double>& par) const {

  assert(par.size() == 4);

  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey
  
  vector<double> model = OIDataM2V (thePositions, par[0], par[1], par[2], sex, par[3], dx, m2_y);

  double chi2 = 0.;
  int nmeas = theMeasurements.size();
  double nmeas_d = theMeasurements.size();
  for(int n = 0; n < nmeas; n++) {
    chi2 += ((model[n]-theMeasurements[n])/theMstds[n])*((model[n]-theMeasurements[n])/theMstds[n]);
  }
  double redchi2 = chi2 / (nmeas_d-4.0-1.0);
  #if PRINTOUT
  std::cout<<redchi2<<"\t\t"<<par[0]<<"\t"<<par[1]<<"\t"<<par[2]<<"\t"<<par[3]<<std::endl;
  #endif
  return chi2;
}

void OIM2VFcn::init()
{
  sex  = ips.sex;
  dx   = ips.dx;
  m2_y = ips.m2_general_y;
}

double OIM2HFcn::operator()(const std::vector<double>& par) const {

  assert(par.size() == 4);

  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sex
  
  vector<double> model = OIDataM2H (thePositions, par[0], par[1], par[2], par[3], sey, dy, m2_y);

  double chi2 = 0.;
  int nmeas = theMeasurements.size();
  double nmeas_d = theMeasurements.size();
  for(int n = 0; n < nmeas; n++) {
    chi2 += ((model[n]-theMeasurements[n])/theMstds[n])*((model[n]-theMeasurements[n])/theMstds[n]);
  }
  double redchi2 = chi2 / (nmeas_d-4.0-1.0);
  #if PRINTOUT
  std::cout<<redchi2<<"\t\t"<<par[0]<<"\t"<<par[1]<<"\t"<<par[2]<<"\t"<<par[3]<<std::endl;
  #endif
  return chi2;
}

void OIM2HFcn::init()
{
  sey  = ips.sex;
  dy   = ips.dx;
  m2_y = ips.m2_general_y;
}

double GaussVFcn::operator()(const std::vector<double>& par) const {

  assert(par.size() == 4);

  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey
  
  vector<double> model = GaussianDataV (thePositions, par[0], par[1], par[2], par[3]);
  
  double chi2 = 0.;
  int nmeas = theMeasurements.size();
  double nmeas_d = theMeasurements.size();
  for(int n = 0; n < nmeas; n++) {
    chi2 += ((model[n]-theMeasurements[n])/theMstds[n])*((model[n]-theMeasurements[n])/theMstds[n]);
  }
  double redchi2 = chi2 / (nmeas_d-4.0-1.0);
  #if PRINTOUT
  std::cout<<redchi2<<"\t\t"<<par[0]<<"\t"<<par[1]<<"\t"<<par[2]<<"\t"<<par[3]<<std::endl;
  #endif
  return chi2;
}

void GaussVFcn::init()
{
  int i = 0;
}



double OICoordEmpiricalFcn::operator()(const std::vector<double>& par) const {

  assert(par.size() == 6);
  
  //par[0] = sex
  //par[1] = sey
  //par[2] = dx0
  //par[3] = dy0
  //par[4] = offset
  //par[5] = amplitude

  vector<double> model = OIDataCoordE (theCoords, par[0], par[1], par[2], par[3], par[4], par[5]);
  
  //plot the mean of the model - if it's way off in oi then it'll be super small...

  double chi2 = 0.;
  int nmeas = theMeasurements.size();
  double nmeas_d = theMeasurements.size();
  for(int n = 0; n < nmeas; n++) {
    chi2 += ((model[n]-theMeasurements[n])/theMstds[n])*((model[n]-theMeasurements[n])/theMstds[n]);
  }
  double redchi2 = chi2 / (nmeas_d-4.0-1.0);
  #if PRINTOUT
  std::cout<<redchi2<<"\t\t"<<par[0]<<"\t"<<par[1]<<"\t"<<par[2]<<"\t"<<par[3]<<"\t"<<par[4]<<"\t"<<par[5]<<std::endl;
  #endif
  return chi2;
}

void OICoordEmpiricalFcn::init()
{
  #if PRINTOUT
  std::cout<<"Fitting Constructor >> OI 2D approach, empirical fitting"<<std::endl;
  #endif
}

