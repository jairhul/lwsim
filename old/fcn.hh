#ifndef MN_Fcn_H_
#define MN_Fcn_H_

#include "config.hh"
#include "Minuit/FCNBase.h"
#include "oi.hh"
#include <cstdlib>
#include <cassert>
#include <vector>
#include <iostream>
#include "assert.h"

extern input_parameters ips;

class OIEmpiricalVFcn : public FCNBase {
public:

  double sex;
  double dx;
  
  OIEmpiricalVFcn(const std::vector<double>& pos,
	  const std::vector<double>& meas,
	  const std::vector<double>& mstd) : theMeasurements(meas),
					     thePositions(pos),
					     theMstds(mstd), 
					     theMin(0.) {init();}
  ~OIEmpiricalVFcn() {}

  virtual void init();

  // Minuit estimates the error by varying each parameter until the function
  // increase by value 'up' defined here.  For Chi2 this is 1 and for negative
  // log-likelihood it is 0.5.
  virtual double up() const {return 1.;}
  virtual double operator()(const std::vector<double>&) const;
  virtual double errorDef() const {return up();}
  
  std::vector<double> measurements() const {return theMeasurements;}
  std::vector<double> positions() const {return thePositions;}
  std::vector<double> stds() const {return theMstds;}
  
private:

  std::vector<double> theMeasurements;
  std::vector<double> thePositions;
  std::vector<double> theMstds;
  double theMin;
};

class OIM2VFcn : public FCNBase {
public:

  double sex;
  double dx;
  double m2_y;
  
  OIM2VFcn(const std::vector<double>& pos,
	  const std::vector<double>& meas,
	  const std::vector<double>& mstd) : theMeasurements(meas),
					     thePositions(pos),
					     theMstds(mstd), 
					     theMin(0.) {init();}
  ~OIM2VFcn() {}

  virtual void init();

  // Minuit estimates the error by varying each parameter until the function
  // increase by value up defined here.  For Chi2 this is 1 and for negative
  // log-likelihood it is 0.5.
  virtual double up() const {return 1.;}
  virtual double operator()(const std::vector<double>&) const;
  virtual double errorDef() const {return up();}
  
  std::vector<double> measurements() const {return theMeasurements;}
  std::vector<double> positions() const {return thePositions;}
  std::vector<double> stds() const {return theMstds;}
  
private:

  std::vector<double> theMeasurements;
  std::vector<double> thePositions;
  std::vector<double> theMstds;
  double theMin;
};

class GaussVFcn : public FCNBase {
public:
  
  GaussVFcn(const std::vector<double>& pos,
	  const std::vector<double>& meas,
	  const std::vector<double>& mstd) : theMeasurements(meas),
					     thePositions(pos),
					     theMstds(mstd), 
					     theMin(0.){init();}
  ~GaussVFcn() {}

  virtual void init();

  // Minuit estimates the error by varying each parameter until the function
  // increase by value up defined here.  For Chi2 this is 1 and for negative
  // log-likelihood it is 0.5.
  virtual double up() const {return 1.;}
  virtual double operator()(const std::vector<double>&) const;
  virtual double errorDef() const {return up();}
  
  std::vector<double> measurements() const {return theMeasurements;}
  std::vector<double> positions() const {return thePositions;}
  std::vector<double> stds() const {return theMstds;}
  
private:

  std::vector<double> theMeasurements;
  std::vector<double> thePositions;
  std::vector<double> theMstds;
  double theMin;
};

class OIEmpiricalHFcn : public FCNBase {
public:

  double sey;
  double dy;
  
  OIEmpiricalHFcn(const std::vector<double>& pos,
	  const std::vector<double>& meas,
	  const std::vector<double>& mstd) : theMeasurements(meas),
					     thePositions(pos),
					     theMstds(mstd), 
					     theMin(0.) {init();}
  ~OIEmpiricalHFcn() {}

  virtual void init();

  // Minuit estimates the error by varying each parameter until the function
  // increase by value up defined here.  For Chi2 this is 1 and for negative
  // log-likelihood it is 0.5.
  virtual double up() const {return 1.;}
  virtual double operator()(const std::vector<double>&) const;
  virtual double errorDef() const {return up();}
  
  std::vector<double> measurements() const {return theMeasurements;}
  std::vector<double> positions() const {return thePositions;}
  std::vector<double> stds() const {return theMstds;}
  
private:

  std::vector<double> theMeasurements;
  std::vector<double> thePositions;
  std::vector<double> theMstds;
  double theMin;
};

class OIM2HFcn : public FCNBase {
public:

  double sey;
  double dy;
  double m2_y;
  
  OIM2HFcn(const std::vector<double>& pos,
	  const std::vector<double>& meas,
	  const std::vector<double>& mstd) : theMeasurements(meas),
					     thePositions(pos),
					     theMstds(mstd), 
					     theMin(0.) {init();}
  ~OIM2HFcn() {}

  virtual void init();

  // Minuit estimates the error by varying each parameter until the function
  // increase by value up defined here.  For Chi2 this is 1 and for negative
  // log-likelihood it is 0.5.
  virtual double up() const {return 1.;}
  virtual double operator()(const std::vector<double>&) const;
  virtual double errorDef() const {return up();}
  
  std::vector<double> measurements() const {return theMeasurements;}
  std::vector<double> positions() const {return thePositions;}
  std::vector<double> stds() const {return theMstds;}
  
private:

  std::vector<double> theMeasurements;
  std::vector<double> thePositions;
  std::vector<double> theMstds;
  double theMin;
};

class OICoordEmpiricalFcn : public FCNBase {
public:
  
  OICoordEmpiricalFcn(const std::vector<vector<double> >& coords,
		      const std::vector<double>& meas,
		      const std::vector<double>& mstd) : theMeasurements(meas),
					     theCoords(coords),
					     theMstds(mstd), 
					     theMin(0.) {init();}
  ~OICoordEmpiricalFcn() {}

  virtual void init();

  // Minuit estimates the error by varying each parameter until the function
  // increase by value 'up' defined here.  For Chi2 this is 1 and for negative
  // log-likelihood it is 0.5.
  virtual double up() const {return 1.;}
  virtual double operator()(const std::vector<double>&) const;
  virtual double errorDef() const {return up();}
  
  std::vector<double> measurements() const {return theMeasurements;}
  std::vector<vector<double> > positions() const {return theCoords;}
  std::vector<double> stds() const {return theMstds;}
  
private:

  std::vector<double> theMeasurements;
  std::vector<vector<double> > theCoords;
  std::vector<double> theMstds;
  double theMin;
};






#endif //MN_Fcn_H_

