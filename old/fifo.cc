//Laser-wire Overlap Integral IO Program
//Version:  2
//Date:     2013_02_17
//Author:   L Nevay

#include "fifo.hh"

void Fifo::ReadParameters (string filepath)
{
  std::cout<<"Reading input parameters"<<std::endl;
  //0 Load file - return if no such file
  std::ifstream ifs(filepath.c_str());
  if (!(ifs)) 
    {
      std::cout << "File not there" << std::endl;
      return;
    }
  else 
    {
      //1 Read Parameters and put in struct
      //construct vector of lines
      vector<string> lines;
      string current_line;
      while (getline(ifs, current_line))
	{lines.push_back(current_line);}
      
      //split each line into key then value (only one in this file type)
      //this is terribly hard coded :(
      //but can't address struct member by string name or compare to string
      //therefore can't iterate through ips and put into appropriate member
      //fortunatley file structure is VERY fixed
      std::string key, s_value;
      std::stringstream ss;
      std::stringstream ss0(lines[0]);
      ss0 >> key >> s_value;
      ips.m2_z         = atof(s_value.c_str());
      std::stringstream ss1(lines[1]);
      ss1 >> key >> s_value;
      ips.m2_y         = atof(s_value.c_str());
      std::stringstream ss2(lines[2]);
      ss2 >> key >> s_value;
      ips.z_o_z        = atof(s_value.c_str());
      std::stringstream ss3(lines[3]);
      ss3 >> key >> s_value;
      ips.z_o_y        = atof(s_value.c_str());
      std::stringstream ss4(lines[4]);
      ss4 >> key >> s_value;
      ips.sigma_o_z    = atof(s_value.c_str());
      std::stringstream ss5(lines[5]);
      ss5 >> key >> s_value;
      ips.sigma_o_y    = atof(s_value.c_str());
      std::stringstream ss6(lines[6]);
      ss6 >> key >> s_value;
      ips.theta        = atof(s_value.c_str());
      std::stringstream ss7(lines[7]);
      ss7 >> key >> s_value;
      ips.m2_general_y = atof(s_value.c_str());
      std::stringstream ss8(lines[8]);
      ss8 >> key >> s_value;
      ips.wavelength   = atof(s_value.c_str());
      std::stringstream ss9(lines[9]);
      ss9 >> key >> s_value;
      ips.sex          = atof(s_value.c_str());
      std::stringstream ss10(lines[10]);
      ss10 >> key >> s_value;
      ips.dx           = atof(s_value.c_str());
    };
};

void Fifo::WriteFitParameters (fit_parameters resultstowrite, string filename)
{
  #if PRINTOUT
  std::cout<<"Writing ouput parameters to "<<filename<<std::endl;
  #endif
  std::ofstream fpf;
  fpf.open(filename.c_str());
  fpf << "centre> "    << std::fixed << std::setprecision(6) << resultstowrite.centre    <<" "<<resultstowrite.centre_err    <<std::endl;
  fpf << "offset> "    << std::fixed << std::setprecision(6) << resultstowrite.offset    <<" "<<resultstowrite.offset_err    <<std::endl;
  fpf << "sigma> "     << std::fixed << std::setprecision(6) << resultstowrite.sigma     <<" "<<resultstowrite.sigma_err     <<std::endl;
  fpf << "amplitude> " << std::fixed << std::setprecision(6) << resultstowrite.amplitude <<" "<<resultstowrite.amplitude_err <<std::endl;
  fpf << "metric> "    << std::fixed << std::setprecision(6) << resultstowrite.metric    << std::endl;
  fpf.close();
}

void Fifo::WriteFitParameters2 (fit_parameters3 rtw, string filename)
{ 
  std::cout<<"Writing ouput parameters to "<<filename<<std::endl;
  std::ofstream fpf;
  fpf.open(filename.c_str());
  fpf<<"sex> "   <<std::fixed<<std::setprecision(6)<<rtw.sex.first<<" "<<(rtw.sex.second).first<<" "<<(rtw.sex.second).second<<std::endl;
  fpf<<"sey> "   <<std::fixed<<std::setprecision(6)<<rtw.sey.first<<" "<<(rtw.sey.second).first<<" "<<(rtw.sey.second).second<<std::endl;
  fpf<<"dx0> "   <<std::fixed<<std::setprecision(6)<<rtw.dx0.first<<" "<<(rtw.dx0.second).first<<" "<<(rtw.dx0.second).second<<std::endl;
  fpf<<"dy0> "   <<std::fixed<<std::setprecision(6)<<rtw.dy0.first<<" "<<(rtw.dy0.second).first<<" "<<(rtw.dy0.second).second<<std::endl;
  fpf<<"offset> "<<std::fixed<<std::setprecision(6)<<rtw.offset.first<<" "<<(rtw.offset.second).first<<" "<<(rtw.offset.second).second<<std::endl;
  fpf<<"amplitude> " <<std::fixed<<std::setprecision(6)<<rtw.amplitude.first<<" "<<(rtw.amplitude.second).first<<" "<<(rtw.amplitude.second).second <<std::endl;
  fpf<< "metric> "<<std::fixed<<std::setprecision(6)<<rtw.metric<<std::endl;
  fpf.close();
}

void Fifo::WriteFitParameters3 (fit_parameters4 rtw, string filename)
{ 
  std::cout<<"Writing ouput parameters to "<<filename<<std::endl;
  std::ofstream fpf;
  fpf.open(filename.c_str());
  fpf<< "centre> "<<std::fixed<<std::setprecision(6)<<rtw.centre.first<<" "<<(rtw.centre.second).first<<" "<<(rtw.centre.second).second<<std::endl;
  fpf<< "offset> "<<std::fixed<<std::setprecision(6)<<rtw.offset.first<<" "<<(rtw.offset.second).first<<" "<<(rtw.offset.second).second<<std::endl;
  fpf<< "sigma> " <<std::fixed<<std::setprecision(6)<<rtw.sigma.first<<" "<<(rtw.sigma.second).first<<" "<<(rtw.sigma.second).second<<std::endl;
  fpf<< "amplitude> "<<std::fixed<<std::setprecision(6)<<rtw.amplitude.first<<" "<<(rtw.amplitude.second).first<<" "<<(rtw.amplitude.second).second<<std::endl;
  fpf << "metric> "<<std::fixed<<std::setprecision(6)<<rtw.metric<<std::endl;
  fpf.close();
}


void Fifo::ReadSingleOI (string filepath)
{
  // want dx, dy, sex, sey
  std::cout<<"Reading Single OI input parameters"<<std::endl;
  //0 Load file - return if no such file
  std::ifstream ifs(filepath.c_str());
  if (!(ifs)) 
    {
      std::cout << "File not there" << std::endl;
      return;
    }
  else 
    {
      //1 Read Parameters and put in struct
      //construct vector of lines
      vector<string> lines;
      string current_line;
      while (getline(ifs, current_line))
	{lines.push_back(current_line);}
      
      //split each line into key then value (only one in this file type)
      //this is terribly hard coded :(
      //but can't address struct member by string name or compare to string
      //therefore can't iterate through ips and put into appropriate member
      //fortunatley file structure is VERY fixed
      std::string key, s_value;
      std::stringstream ss;
      std::stringstream ss0(lines[0]);
      ss0 >> key >> s_value;
      sv.dx        = atof(s_value.c_str());
      std::stringstream ss1(lines[1]);
      ss1 >> key >> s_value;
      sv.dy         = atof(s_value.c_str());
      std::stringstream ss2(lines[2]);
      ss2 >> key >> s_value;
      sv.sex        = atof(s_value.c_str());
      std::stringstream ss3(lines[3]);
      ss3 >> key >> s_value;
      sv.sey        = atof(s_value.c_str());
      std::stringstream ss4(lines[4]);
      ss4 >> key >> s_value;
      sv.m2         = atof(s_value.c_str());
    };
}


void Fifo::WriteSingleOI (double result, string filename)
{
  std::cout<<"Writing SingleOI to  "<<filename<<std::endl;
  std::ofstream fpf;
  fpf.open(filename.c_str());
  fpf << "oi> "    << std::fixed << std::setprecision(6) << result<<std::endl;
  fpf.close();
}

void Fifo::ReadSingleVectorOI (string filepath)
{
  std::cout<<"Reading Single OI Vector input parameters"<<std::endl;
  //0 Load file - return if no such file
  std::ifstream ifs(filepath.c_str());
  if (!(ifs)) 
    {
      std::cout << "File not there" << std::endl;
      return;
    }
  else 
    {
      //1 Read Parameters and put in struct
      //construct vector of lines
      vector<string> lines;
      string current_line;
      while (getline(ifs, current_line))
	{lines.push_back(current_line);}
      
      //split each line into key then value (only one in this file type)
      //this is terribly hard coded :(
      //but can't address struct member by string name or compare to string
      //therefore can't iterate through ips and put into appropriate member
      //fortunatley file structure is VERY fixed
      string key, s_value, tempstring;
      std::stringstream ss0(lines[0]);
      while (ss0 >> tempstring)
	{
	  sv.x.push_back(atof(tempstring.c_str()));
	}
      
      sv.x.erase(sv.x.begin());
      
      std::stringstream ss1(lines[1]);
      ss1 >> key >> s_value;
      sv.dx        = atof(s_value.c_str());
      std::stringstream ss2(lines[2]);
      ss2 >> key >> s_value;
      sv.dy         = atof(s_value.c_str());
      std::stringstream ss3(lines[3]);
      ss3 >> key >> s_value;
      sv.sex        = atof(s_value.c_str());
      std::stringstream ss4(lines[4]);
      ss4 >> key >> s_value;
      sv.sey        = atof(s_value.c_str());
      std::stringstream ss5(lines[5]);
      ss5 >> key >> s_value;
      sv.amplitude  = atof(s_value.c_str());
      std::stringstream ss6(lines[6]);
      ss6 >> key >> s_value;
      sv.offset     = atof(s_value.c_str());
      std::stringstream ss7(lines[7]);
      ss7 >> key >> s_value;
      sv.centre     = atof(s_value.c_str());
      std::stringstream ss8(lines[8]);
      ss8 >> key >> s_value;
      sv.m2         = atof(s_value.c_str());
    };
}

void Fifo::WriteSingleVectorOI (vector<double> results, string filename)
{
  std::cout<<"Writing SingleOI Vector to  "<<filename<<std::endl;
  std::ofstream fpf;
  fpf.open(filename.c_str());
  fpf << "oi> ";
  for (vector<double>::iterator i = results.begin(); i != results.end(); ++i)
    {fpf << *i << " ";}
  fpf << std::endl;
  fpf.close();
}

void Fifo::ReadInputData (string filename)
{
  std::cout<<"Reading input data"<<std::endl;
  //0 Load file - return if no such file
  std::ifstream ifs(filename.c_str());
  if (!(ifs)) 
    {
      std::cout << "File not there..." << std::endl;
      return;
    }
  else 
    {
      //1 Read Parameters and put in struct
      //construct vector of lines
      vector<string> lines;
      string current_line;
      while (getline(ifs, current_line))
	{lines.push_back(current_line);}
      string key, s_value, tempstring;
      std::stringstream ss1(lines[0]);      
      while (ss1 >> tempstring)
	{
	  data_input.y.push_back(atof(tempstring.c_str()));
	}
	
      data_input.y.erase(data_input.y.begin());
      std::stringstream ss2(lines[1]);
      while (ss2 >> tempstring)
	{
	  data_input.signal.push_back(atof(tempstring.c_str()));
	}
      data_input.signal.erase(data_input.signal.begin());
      
      std::stringstream ss3(lines[2]);
      while (ss3 >> tempstring)
	{
	  data_input.signal_err.push_back(atof(tempstring.c_str()));
	  }
      data_input.signal_err.erase(data_input.signal_err.begin());
    };
}

void Fifo::ReadInputData2 (string filename)
{
  //puts data from python into fid_data2 data struct
  
  std::cout<<"Reading input data"<<std::endl;
  //0 Load file - return if no such file
  std::ifstream ifs(filename.c_str());
  if (!(ifs)) 
    {
      std::cout << "File not there..." << std::endl;
      return;
    }
  else 
    {
      //1 Read Parameters and put in struct
      //construct vector of lines
      vector<string> lines;
      string current_line;
      while (getline(ifs, current_line))
	{lines.push_back(current_line);}
      string key, s_value, tempstring;
      
      //x
      std::stringstream ss1(lines[0]);      
      while (ss1 >> tempstring)
	{
	  data_input2.x.push_back(atof(tempstring.c_str()));
	}
      data_input2.x.erase(data_input2.x.begin());
      
      //y
      std::stringstream ss2(lines[1]);      
      while (ss2 >> tempstring)
	{
	  data_input2.y.push_back(atof(tempstring.c_str()));
	}
      data_input2.y.erase(data_input2.y.begin());
      
      //signal
      std::stringstream ss3(lines[2]);
      while (ss3 >> tempstring)
	{
	  data_input2.signal.push_back(atof(tempstring.c_str()));
	}
      data_input2.signal.erase(data_input2.signal.begin());
      
      //signal_err
      std::stringstream ss4(lines[3]);
      while (ss4 >> tempstring)
	{
	  data_input2.signal_err.push_back(atof(tempstring.c_str()));
	  }
      data_input2.signal_err.erase(data_input2.signal_err.begin());

      //zip x and y together for passing
      vector<double > tempholder2;
      for (int i = 0; i < data_input2.x.size(); i++)
	{
	  tempholder2.push_back(data_input2.x[i]);
	  tempholder2.push_back(data_input2.y[i]);
	  coords_input.push_back(tempholder2);
	  tempholder2.clear();
	}

    };
}

void Fifo::WriteOutputData (fit_data results, string filename)
{ 
  std::cout<<"Writing output data to " <<filename<<std::endl;
  std::ofstream rf;
  rf.open(filename.c_str());
  rf << "y> ";
  for (vector<double>::iterator i = results.y.begin(); i != results.y.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "signal> ";
  for (vector<double>::iterator i = results.signal.begin(); i != results.signal.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "signal_err> ";
  for (vector<double>::iterator i = results.signal_err.begin(); i != results.signal_err.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "model> ";
  for (vector<double>::iterator i = results.model.begin(); i != results.model.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf.close();
}

void Fifo::WriteOutputData2 (fit_data2 results, string filename)
{ 
  //std::cout<<results.x.size()<< " " << results.y.size()<<" "<<results.signal.size()<<" "<<results.signal_err.size()<<" "<<results.x_model.size()<<" "<<results.y_model.size()<<" "<<results.signal_model.size()<<std::endl;
  std::cout<<"Writing output data to " <<filename<<std::endl;
  std::ofstream rf;
  rf.open(filename.c_str());
  rf << "x> ";
  for (vector<double>::iterator i = results.x.begin(); i != results.x.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "y> ";
  for (vector<double>::iterator i = results.y.begin(); i != results.y.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "signal> ";
  for (vector<double>::iterator i = results.signal.begin(); i != results.signal.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "signal_err> ";
  for (vector<double>::iterator i = results.signal_err.begin(); i != results.signal_err.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "x_model> ";
  for (vector<double>::iterator i = results.x_model.begin(); i != results.x_model.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "y_model> ";
  for (vector<double>::iterator i = results.y_model.begin(); i != results.y_model.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "signal_model> ";
  for (vector<double>::iterator i = results.signal_model.begin(); i != results.signal_model.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf.close();
}

void Fifo::WriteSurfaceData (surface_data sd, string filename)
{ 
  std::cout<<"Writing output data to " <<filename<<std::endl;
  std::ofstream rf;
  rf.open(filename.c_str());
  rf << "x> ";
  for (vector<double>::iterator i = sd.x.begin(); i != sd.x.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "y> ";
  for (vector<double>::iterator i = sd.y.begin(); i != sd.y.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf << "signal> ";
  for (vector<double>::iterator i = sd.s.begin(); i != sd.s.end(); ++i)
    {rf << *i << " ";}
  rf << std::endl;
  rf.close();
}



void Fifo::PrintStructs ()
{
  std::cout << "Input fit parameters" << std::endl;
  std::cout << "m2_z         " << ips.m2_z         << std::endl;
  std::cout << "m2_y         " << ips.m2_y         << std::endl;
  std::cout << "z_o_z        " << ips.z_o_z        << std::endl;
  std::cout << "z_o_y        " << ips.z_o_y        << std::endl;
  std::cout << "sigma_o_z    " << ips.sigma_o_z    << std::endl;
  std::cout << "sigma_o_z    " << ips.sigma_o_y    << std::endl;
  std::cout << "theta        " << ips.theta        << std::endl;
  std::cout << "m2 general   " << ips.m2_general_y << std::endl;
  std::cout << "wavelength   " << ips.wavelength   << std::endl;
  std::cout << "sex          " << ips.sex          << std::endl;
  std::cout << "dx           " << ips.dx           << std::endl;
  std::cout << std::endl;

  std::cout << "Single OI parameters" << std::endl;
  std::cout << "dx           " << sv.dx        << std::endl;
  std::cout << "dy           " << sv.dy        << std::endl;
  std::cout << "sex          " << sv.sex       << std::endl;
  std::cout << "sey          " << sv.sey       << std::endl;
  std::cout << "amplitude    " << sv.amplitude << std::endl;
  std::cout << "off          " << sv.offset    << std::endl;
  std::cout << "cen          " << sv.centre    << std::endl;
  std::cout << "m2           " << sv.m2        << std::endl;
  std::cout << std::endl;

  std::cout << "Vector part of Single OI parameters" << std::endl;
  for (vector<double>::iterator i = sv.x.begin(); i != sv.x.end(); ++i)
    {
      std::cout << *i << "\t";
    }
  std::cout<<std::endl<<std::endl;

  std::cout << "Fit input data" << std::endl;
  std::cout << "Y" << std::endl;
  for (vector<double>::iterator i = data_input.y.begin(); i != data_input.y.end(); ++i)
    {std::cout << *i << "\t";}
  std::cout << std::endl << std::endl;
  std::cout << "Signal" << std::endl;
  for (vector<double>::iterator i = data_input.signal.begin(); i != data_input.signal.end(); ++i)
    {std::cout << *i << "\t";}
  std::cout << std::endl << std::endl;
  std::cout << "Signal std" << std::endl;
  for (vector<double>::iterator i = data_input.signal_err.begin(); i != data_input.signal_err.end(); ++i)
    {std::cout << *i << "\t";}
  std::cout << std::endl;
}

void Fifo::PrintInputData2 ()
{
  std::cout<<"Input Data 2"<<std::endl;
  for (vector<double>::iterator i = data_input2.x.begin(); i != data_input2.x.end(); ++i)
    {
      std::cout<<*i<<" ";
    }
  std::cout<<std::endl;
  for (vector<double>::iterator i = data_input2.y.begin(); i != data_input2.y.end(); ++i)
    {
      std::cout<<*i<<" ";
    }
  std::cout<<std::endl;
  for (vector<double>::iterator i = data_input2.signal.begin(); i != data_input2.signal.end(); ++i)
    {
      std::cout<<*i<<" ";
    }
  std::cout<<std::endl;
  for (vector<double>::iterator i = data_input2.signal_err.begin(); i != data_input2.signal_err.end(); ++i)
    {
      std::cout<<*i<<" ";
    }
  std::cout<<std::endl;

}

//for testing purposes
/*
int main ()
{
  Fifo a;
  a.ReadParameters("../fit_parameters_input.dat");
  a.ReadInputData("../fit_data_input.dat");
  a.ReadSingleOI("../singleoi_input.dat");
  a.ReadSingleVectorOI("../singleoivector_input.dat");
  a.ReadInputData2("../fit_data_input.dat");
  a.PrintStructs();
  a.PrintInputData2();
  //a.WriteOutputData(a.data_input);
  return 0;
}
*/


