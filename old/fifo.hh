#ifndef FIFO_H_
#define FIFO_H_

#include "config.hh"
#include "mystructs.hh"

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

using std::string;
using std::vector;

class Fifo {
public:
  //class data objects
  input_parameters ips;
  fit_data         data_input;
  fit_parameters   params_input;
  fit_data         data_output;
  fit_parameters   params_output;
  func_parameters  si;
  single_params    sv;
  fit_data2        data_input2;
  
  vector<vector<double> > coords_input;
  
  //class methods
  void ReadParameters      (string filename);
  void WriteFitParameters  (fit_parameters resultstowrite, string filename);
  void WriteFitParameters2 (fit_parameters3 rtw, string filename);
  void WriteFitParameters3 (fit_parameters4 rtw, string filename);
  
  void ReadSingleOI        (string filename);
  void WriteSingleOI       (double result, string filename);

  void ReadSingleVectorOI  (string filename);
  void WriteSingleVectorOI (vector<double> results, string filename);
  
  void ReadInputData       (string filename);
  void ReadInputData2      (string filename);
  void WriteOutputData     (fit_data results, string filename);
  void WriteOutputData2    (fit_data2 results, string filename);
  void WriteSurfaceData    (surface_data sd, string filename);
  
  void PrintStructs ();
  void PrintInputData2 ();
};

#endif /*FIFO_H_*/
