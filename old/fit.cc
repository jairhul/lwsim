//Laser-wire Overlap Integral Fitting Program
//Version:  1
//Date:     2013_01_03
//Author:   L Nevay

#include "fit.hh"

double WeightedMean (vector<double> x, vector<double> weights)
{
  int sz_x, sz_w;
  double min, nom, denom, r;
  sz_x   = x.size();
  sz_w   = weights.size();

  if (sz_x != sz_w)
    {
      int middle = sz_x/2;
      r = middle;
    }
  else 
    {
      nom   = 0;
      denom = 0;
      for (int i = 0; i < sz_x; i++)
	{
	  nom   += (x[i]*weights[i]);
	  denom += weights[i];
	}
      r    = nom / denom;
    }
   return r;
};

fit_parameters GaussInitial (vector<double> y, vector<double> signal)
{
  double max_y, min_y, max_s, min_s;
  double amplitude, offset, centre, sigma;
  max_y = *max_element(y.begin(), y.end());
  min_y = *min_element(y.begin(), y.end());
  max_s = *max_element(signal.begin(), signal.end());
  min_s = *min_element(signal.begin(), signal.end());

  amplitude = max_s - min_s;
  offset    = min_s;
  vector<double> s_high_power;
  for (vector<double>::iterator i = signal.begin(); i != signal.end(); ++i)
    {
      s_high_power.push_back(pow(*i,3));
    }
  centre    = WeightedMean(y,s_high_power);
  sigma     = (max_y - min_y)*0.08;

  fit_parameters initial;
  initial.centre    = centre;
  initial.offset    = offset;
  initial.sigma     = sigma;
  initial.amplitude = amplitude;
  initial.metric    = 1.0;

  return initial;
};

void FitGaussV (vector<double> y, vector<double> signal, vector<double> signal_err)
{
  #if PRINTOUT
  std::cout<<"Fitting to Gaussian model"<<std::endl<<std::endl;
  //calculate initial parameters
  std::cout << "Calculating initial values" << std::endl;
  #endif
  fit_parameters initial = GaussInitial(y, signal);

  #if PRINTOUT
  std::cout<<"Initial Amplitude: "<<initial.amplitude <<std::endl;
  std::cout<<"Initial Offset:    "<<initial.offset    <<std::endl;
  std::cout<<"Initial Centre:    "<<initial.centre    <<std::endl;
  std::cout<<"Initial Sigma:     "<<initial.sigma     <<std::endl;
  #endif
  
  //construct the function
  GaussVFcn theFCN(y, signal, signal_err);

  //construct initial parameters
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey
  MnUserParameters upar;
  upar.add("amplitude", initial.amplitude, 0.1);
  upar.add("offset", initial.offset, 0.1);
  upar.add("centre", initial.centre, 0.1);
  upar.add("sigma", initial.sigma, 0.1);

  //set parameter limits
  upar.setLimits("sigma", 0.1, 1000.0);
  //upar.setLimits("amplitude", 0.00001,1000.0);
  
  //construct MIGRAD minimiser
  MnMigrad migrad(theFCN, upar);
  
  //minimise
  
  std::cout << "Starting minimisation" << std::endl;
  #if PRINTOUT
  std::cout << "RedChi2\t\tAmplitude\tOffset\tCentre\tSigma"<<std::endl;
  #endif
  FunctionMinimum min = migrad();
  std::cout << "Finished minimisation" << std::endl;

  //estimate errors using MINOS
  std::cout << "Estimating uncertainties using Minos" << std::endl;
  MnMinos minos(theFCN, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;
  #if PRINTOUT
  std::cout << "Amplitude uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_amp = minos(0,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Offset uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_off = minos(1,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Centre uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_cen = minos(2,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Sigma_{ey} uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_sey = minos(3,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Finished uncertainties:" << std::endl;
  #endif

  double rchi2 = min.fval() / (y.size()-4.0-1.0);
  //report the final fit values
  #if PRINTOUT
  std::cout << "Fitted values are:\t"<<std::endl;
  std::cout << "Reduced Chi2:\t"<< rchi2<<std::endl;
  std::cout << "amplitude:\t"   << min.userState().value("amplitude") << "\t+ " << e_amp.first << "\t- " << e_amp.second << std::endl;
  std::cout << "offset:\t\t"    << min.userState().value("offset")    << "\t+ " << e_off.first << "\t- " << e_off.second << std::endl;
  std::cout << "centre:\t\t"    << min.userState().value("centre")    << "\t+ " << e_cen.first << "\t- " << e_cen.second << std::endl;
  std::cout << "sigma:\t\t"     << min.userState().value("sigma")     << "\t+ " << e_sey.first << "\t- " << e_sey.second << std::endl;
  #endif
  
  oi_parameters_output.amplitude = min.userState().value("amplitude");
  oi_parameters_output.offset    = min.userState().value("offset");
  oi_parameters_output.centre    = min.userState().value("centre");
  oi_parameters_output.sigma     = min.userState().value("sigma");
  oi_parameters_output.metric    = rchi2;

  oi_parameters_output.amplitude_err = std::max(e_amp.first,e_amp.second);
  oi_parameters_output.offset_err    = std::max(e_off.first,e_off.second);
  oi_parameters_output.centre_err    = std::max(e_cen.first,e_cen.second);
  oi_parameters_output.sigma_err     = std::max(e_sey.first,e_sey.second);
  
  double max_y, min_y, stepsize, pos;
  int npoints = 500;
  int i = 0;
  vector<double> ymodel;
  max_y = *max_element(y.begin(), y.end());
  min_y = *min_element(y.begin(), y.end());
  stepsize = ( max_y - min_y ) / double (npoints);
  pos = min_y;
  while (i < npoints)
    {
      ymodel.push_back(pos);
      pos += stepsize;
      i++;
    }

  data_output.y          = ymodel;
  data_output.signal     = signal;
  data_output.signal_err = signal_err;
  data_output.model      = GaussianDataV(ymodel, oi_parameters_output.amplitude, oi_parameters_output.offset, oi_parameters_output.centre, oi_parameters_output.sigma);

}

void FitOIEmpiricalV (vector<double> y, vector<double> signal, vector<double> signal_err)
{
  #if PRINTOUT
  std::cout<<"Fitting to Overlap Integral - Empirical model"<<std::endl<<std::endl;
  //calculate initial parameters
  std::cout << "Calculating initial values" << std::endl;
  #endif
  fit_parameters initial = GaussInitial(y, signal);
  initial.sigma = initial.sigma*0.3;

  #if PRINTOUT
  std::cout<<"Initial Amplitude: "<<initial.amplitude <<std::endl;
  std::cout<<"Initial Offset:    "<<initial.offset    <<std::endl;
  std::cout<<"Initial Centre:    "<<initial.centre    <<std::endl;
  std::cout<<"Initial Sigma:     "<<initial.sigma     <<std::endl;
  #endif
  
  //construct the function
  OIEmpiricalVFcn theFCN(y, signal, signal_err);

  //construct initial parameters
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey
  MnUserParameters upar;
  upar.add("amplitude", initial.amplitude, 0.2);
  upar.add("offset", initial.offset, 0.01);
  upar.add("centre", initial.centre, 1.0);
  upar.add("sigma", initial.sigma, 0.1);

  //set parameter limits
  upar.setLimits("sigma", 0.1, 200.0);
  upar.setLimits("amplitude", 0.01,20000.0);
  
  //construct MIGRAD minimiser
  MnMigrad migrad(theFCN, upar);
  
  //minimise
  std::cout << "Starting minimisation" << std::endl;
  #if PRINTOUT
  std::cout << "RedChi2\t\tAmplitude\tOffset\tCentre\tSigma"<<std::endl;
  #endif
  FunctionMinimum min = migrad();
  std::cout << "Finished minimisation" << std::endl;

  //estimate errors using MINOS
  std::cout << "Estimating uncertainties using Minos" << std::endl;
  MnMinos minos(theFCN, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;
  #if PRINTOUT
  std::cout << "Amplitude uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_amp = minos(0,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Offset uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_off = minos(1,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Centre uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_cen = minos(2,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Sigma_{ey} uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_sey = minos(3,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Finished uncertainties:" << std::endl;
  #endif

  double rchi2 = min.fval() / (y.size()-4.0-1.0);
  
  //report the final fit values
  #if PRINTOUT
  std::cout << "Fitted values are:\t"<<std::endl;
  std::cout << "Reduced Chi2:\t"<< rchi2<<std::endl;
  std::cout << "amplitude:\t"   << min.userState().value("amplitude") << "\t+ " << e_amp.first << "\t- " << e_amp.second << std::endl;
  std::cout << "offset:\t\t"    << min.userState().value("offset")    << "\t+ " << e_off.first << "\t- " << e_off.second << std::endl;
  std::cout << "centre:\t\t"    << min.userState().value("centre")    << "\t+ " << e_cen.first << "\t- " << e_cen.second << std::endl;
  std::cout << "sigma:\t\t"     << min.userState().value("sigma")     << "\t+ " << e_sey.first << "\t- " << e_sey.second << std::endl;
  #endif
  
  oi_parameters_output3.amplitude = std::make_pair(min.userState().value("amplitude"), e_amp);
  oi_parameters_output3.offset    = std::make_pair(min.userState().value("offset"), e_off);
  oi_parameters_output3.centre    = std::make_pair(min.userState().value("centre"), e_cen);
  oi_parameters_output3.sigma     = std::make_pair(min.userState().value("sigma"), e_sey);
  oi_parameters_output3.metric    = rchi2;
  
  //generate model x points
  double max_y, min_y, stepsize, pos;
  int npoints = MODEL_NPOINTS;
  int i = 0;
  vector<double> ymodel;
  max_y = *max_element(y.begin(), y.end());
  min_y = *min_element(y.begin(), y.end());
  stepsize = ( max_y - min_y ) / double (npoints);
  pos = min_y;
  while (i < npoints)
    {
      ymodel.push_back(pos);
      pos += stepsize;
      i++;
    }

  //export signal data plus fitted model data
  data_output.y          = ymodel;
  data_output.signal     = signal;
  data_output.signal_err = signal_err;
  data_output.model      = OIDataEmpiricalV(ymodel, 
					     oi_parameters_output3.amplitude.first, 
					     oi_parameters_output3.offset.first, 
					     oi_parameters_output3.centre.first, 
					     ips.sex, 
					     oi_parameters_output3.sigma.first, 
					     ips.dx);
};

void FitOIEmpiricalH (vector<double> x, vector<double> signal, vector<double> signal_err)
{
  #if PRINTOUT
  std::cout<<"Fitting to Overlap Integral - Empirical model"<<std::endl<<std::endl;
  //calculate initial parameters
  std::cout << "Calculating initial values" << std::endl;
  #endif
  fit_parameters initial = GaussInitial(x, signal);
  initial.sigma = initial.sigma;

  #if PRINTOUT
  std::cout << "Initial Amplitude: " << initial.amplitude << std::endl;
  std::cout << "Initial Offset:    " << initial.offset    << std::endl;
  std::cout << "Initial Centre:    " << initial.centre    << std::endl;
  std::cout << "Initial Sigma:     " << initial.sigma     << std::endl;
  #endif

  //construct the function
  OIEmpiricalHFcn theFCN(x, signal, signal_err);

  //construct initial parameters
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sex
  MnUserParameters upar;
  upar.add("amplitude", initial.amplitude, 0.2);
  upar.add("offset", initial.offset, 0.01);
  upar.add("centre", initial.centre, 1.0);
  upar.add("sigma", initial.sigma, 0.1);

  //set parameter limits
  upar.setLimits("sigma", 20, 1000.0);
  upar.setLimits("amplitude", 0.01,20000.0);
  
  //construct MIGRAD minimiser
  MnMigrad migrad(theFCN, upar);
  
  //minimise
  std::cout << "Starting minimisation" << std::endl;
  #if PRINTOUT
  std::cout << "RedChi2\t\tAmplitude\tOffset\tCentre\tSigma"<<std::endl;
  #endif
  FunctionMinimum min = migrad();
  std::cout << "Finished minimisation" << std::endl;

  //estimate errors using MINOS
  std::cout << "Estimating uncertainties using Minos" << std::endl;
  MnMinos minos(theFCN, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;
  #if PRINTOUT
  std::cout << "Amplitude uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_amp = minos(0,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Offseet uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_off = minos(1,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Centre uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_cen = minos(2,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Sigma_{ex} uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_sex = minos(3,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Finished uncertainties:" << std::endl;
  #endif

  double rchi2 = min.fval() / (x.size()-4.0-1.0);
  //report the final fit values
  #if PRINTOUT
  std::cout << "Fitted values are:\t"<<std::endl;
  std::cout << "Reduced Chi2:\t"<< rchi2<<std::endl;
  std::cout << "amplitude:\t" << min.userState().value("amplitude") << "\t+ " << e_amp.first << "\t- " << e_amp.second << std::endl;
  std::cout << "offset:\t\t"  << min.userState().value("offset")    << "\t+ " << e_off.first << "\t- " << e_off.second << std::endl;
  std::cout << "centre:\t\t"  << min.userState().value("centre")    << "\t+ " << e_cen.first << "\t- " << e_cen.second << std::endl;
  std::cout << "sigma:\t\t"   << min.userState().value("sigma")     << "\t+ " << e_sex.first << "\t- " << e_sex.second << std::endl;
  #endif

  oi_parameters_output3.amplitude = std::make_pair(min.userState().value("amplitude"), e_amp);
  oi_parameters_output3.offset    = std::make_pair(min.userState().value("offset"), e_off);
  oi_parameters_output3.centre    = std::make_pair(min.userState().value("centre"), e_cen);
  oi_parameters_output3.sigma     = std::make_pair(min.userState().value("sigma"), e_sex);
  oi_parameters_output3.metric    = rchi2;
  
  //generate model x points
  double max_x, min_x, stepsize, pos;
  int npoints = 500;
  int i = 0;
  vector<double> xmodel;
  max_x = *max_element(x.begin(), x.end());
  min_x = *min_element(x.begin(), x.end());
  stepsize = ( max_x - min_x ) / double (npoints);
  pos = min_x;
  while (i < npoints)
    {
      xmodel.push_back(pos);
      pos += stepsize;
      i++;
    }
  
  //export signal data plus fitted model data
  data_output.y          = xmodel;
  data_output.signal     = signal;
  data_output.signal_err = signal_err;
  data_output.model      = OIDataEmpiricalH(xmodel, 
					    oi_parameters_output3.amplitude.first, 
					    oi_parameters_output3.offset.first, 
					    oi_parameters_output3.centre.first, 
					    oi_parameters_output3.sigma.first,
					    ips.sex, 
					    0.0);
  //remember ips.sex is actually sey here and there is no facility for dy so it's 0
};

void FitOIM2V (vector<double> y, vector<double> signal, vector<double> signal_err)
{
  #if PRINTOUT
  std::cout<<"Fitting to Overlap Integral - M2 Y only model"<<std::endl<<std::endl;
  //calculate initial parameters
  std::cout << "Calculating initial values" << std::endl;
  #endif
  fit_parameters initial = GaussInitial(y, signal);
  initial.sigma = initial.sigma*0.3;
  
  #if PRINTOUT
  std::cout<<"Initial Amplitude: "<<initial.amplitude <<std::endl;
  std::cout<<"Initial Offset:    "<<initial.offset    <<std::endl;
  std::cout<<"Initial Centre:    "<<initial.centre    <<std::endl;
  std::cout<<"Initial Sigma:     "<<initial.sigma     <<std::endl;
  #endif

  //construct the function
  OIM2VFcn theFCN(y, signal, signal_err);

  //construct initial parameters
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey
  MnUserParameters upar;
  upar.add("amplitude", initial.amplitude, 0.1);
  upar.add("offset", initial.offset, 0.1);
  upar.add("centre", initial.centre, 0.1);
  upar.add("sigma", initial.sigma, 0.1);

  //set parameter limits
  upar.setLimits("sigma", 0.1, 200.0);
  upar.setLimits("amplitude", 0.01,20000.0);
  
  //construct MIGRAD minimiser
  MnMigrad migrad(theFCN, upar);
  
  //minimise
  std::cout << "Starting minimisation" << std::endl;
  #if PRINTOUT
  std::cout << "RedChi2\t\tAmplitude\tOffset\tCentre\tSigma"<<std::endl;
  #endif
  FunctionMinimum min = migrad();
  std::cout << "Finished minimisation" << std::endl;

  //estimate errors using MINOS
  std::cout << "Estimating uncertainties using Minos" << std::endl;
  MnMinos minos(theFCN, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;
  #if PRINTOUT
  std::cout << "Amplitude uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_amp = minos(0,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Offset uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_off = minos(1,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Centre uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_cen = minos(2,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Sigma_{ey} uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_sey = minos(3,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Finished uncertainties:" << std::endl;
  #endif

  double rchi2 = min.fval() / (y.size()-4.0-1.0);
  //report the final fit values
  #if PRINTOUT 
  std::cout << "Fitted values are:\t"<<std::endl;
  std::cout << "Reduced Chi2:\t"<< rchi2<<std::endl;
  std::cout << "amplitude:\t" << min.userState().value("amplitude") << "\t+ " << e_amp.first << "\t- " << e_amp.second << std::endl;
  std::cout << "offset:\t\t"  << min.userState().value("offset")    << "\t+ " << e_off.first << "\t- " << e_off.second << std::endl;
  std::cout << "centre:\t\t"  << min.userState().value("centre")    << "\t+ " << e_cen.first << "\t- " << e_cen.second << std::endl;
  std::cout << "sigma:\t\t"   << min.userState().value("sigma")     << "\t+ " << e_sey.first << "\t- " << e_sey.second << std::endl;
  #endif
  
  oi_parameters_output3.amplitude = std::make_pair(min.userState().value("amplitude"), e_amp);
  oi_parameters_output3.offset    = std::make_pair(min.userState().value("offset"), e_off);
  oi_parameters_output3.centre    = std::make_pair(min.userState().value("centre"), e_cen);
  oi_parameters_output3.sigma     = std::make_pair(min.userState().value("sigma"), e_sey);
  oi_parameters_output3.metric    = rchi2;

  //generate model x points
  double max_y, min_y, stepsize, pos;
  int npoints = 500;
  int i = 0;
  vector<double> ymodel;
  max_y = *max_element(y.begin(), y.end());
  min_y = *min_element(y.begin(), y.end());
  stepsize = ( max_y - min_y ) / double (npoints);
  pos = min_y;
  while (i < npoints)
    {
      ymodel.push_back(pos);
      pos += stepsize;
      i++;
    }

  //export signal data plus fitted model data
  data_output.y          = ymodel;
  data_output.signal     = signal;
  data_output.signal_err = signal_err;
  data_output.model      = OIDataM2V(ymodel, 
				     oi_parameters_output3.amplitude.first, 
				     oi_parameters_output3.offset.first, 
				     oi_parameters_output3.centre.first, 
				     ips.sex, 
				     oi_parameters_output3.sigma.first, 
				     ips.dx,
				     ips.m2_general_y);
};


void FitOIM2H (vector<double> x, vector<double> signal, vector<double> signal_err)
{
  #if PRINTOUT
  std::cout<<"Fitting to Overlap Integral for HORIZONTAL SCAN - M2 Y only model"<<std::endl<<std::endl;
  //calculate initial parameters
  std::cout << "Calculating initial values" << std::endl;
  #endif
  fit_parameters initial = GaussInitial(x, signal);
  initial.sigma = initial.sigma*0.3;
  
  #if PRINTOUT
  std::cout << "Initial Amplitude: " << initial.amplitude << std::endl;
  std::cout << "Initial Offset:    " << initial.offset    << std::endl;
  std::cout << "Initial Centre:    " << initial.centre    << std::endl;
  std::cout << "Initial Sigma:     " << initial.sigma     << std::endl;
  #endif

  //construct the function
  OIM2HFcn theFCN(x, signal, signal_err);

  //construct initial parameters
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey
  MnUserParameters upar;
  upar.add("amplitude", initial.amplitude, 0.1);
  upar.add("offset", initial.offset, 0.1);
  upar.add("centre", initial.centre, 0.1);
  upar.add("sigma", initial.sigma, 0.1);

  //set parameter limits
  upar.setLimits("sigma", 20.0, 1000.0);
  upar.setLimits("amplitude", 0.01,20000.0);
  
  //construct MIGRAD minimiser
  MnMigrad migrad(theFCN, upar);
  
  //minimise
  std::cout << "Starting minimisation" << std::endl;
  #if PRINTOUT
  std::cout << "RedChi2\t\tAmplitude\tOffset\tCentre\tSigma"<<std::endl;
  #endif
  FunctionMinimum min = migrad();
  std::cout << "Finished minimisation" << std::endl;

  //estimate errors using MINOS
  std::cout << "Estimating uncertainties using Minos" << std::endl;
  MnMinos minos(theFCN, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;
  #if PRINTOUT
  std::cout << "Amplitude uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_amp = minos(0,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Offset uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_off = minos(1,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Centre uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_cen = minos(2,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Sigma_{ey} uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_sey = minos(3,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Finished uncertainties" << std::endl;
  #endif

  double rchi2 = min.fval() / (x.size()-4.0-1.0);
  //report the final fit values
  #if PRINTOUT
  std::cout << "Fitted values are:\t"<<std::endl;
  std::cout << "Reduced Chi2:\t"<< rchi2<<std::endl;
  std::cout << "amplitude:\t" << min.userState().value("amplitude") << "\t+ " << e_amp.first << "\t- " << e_amp.second << std::endl;
  std::cout << "offset:\t\t"  << min.userState().value("offset")    << "\t+ " << e_off.first << "\t- " << e_off.second << std::endl;
  std::cout << "centre:\t\t"  << min.userState().value("centre")    << "\t+ " << e_cen.first << "\t- " << e_cen.second << std::endl;
  std::cout << "sigma:\t\t"   << min.userState().value("sigma")     << "\t+ " << e_sey.first << "\t- " << e_sey.second << std::endl;
  #endif
  
  oi_parameters_output.amplitude = min.userState().value("amplitude");
  oi_parameters_output.offset    = min.userState().value("offset");
  oi_parameters_output.centre    = min.userState().value("centre");
  oi_parameters_output.sigma     = min.userState().value("sigma");
  oi_parameters_output.metric    = rchi2;

  oi_parameters_output.amplitude_err = std::max(e_amp.first,e_amp.second);
  oi_parameters_output.offset_err    = std::max(e_off.first,e_off.second);
  oi_parameters_output.centre_err    = std::max(e_cen.first,e_cen.second);
  oi_parameters_output.sigma_err     = std::max(e_sey.first,e_sey.second);
  
  double max_x, min_x, stepsize, pos;
  int npoints = 500;
  int i = 0;
  vector<double> xmodel;
  max_x = *max_element(x.begin(), x.end());
  min_x = *min_element(x.begin(), x.end());
  stepsize = ( max_x - min_x ) / double (npoints);
  pos = min_x;
  while (i < npoints)
    {
      xmodel.push_back(pos);
      pos += stepsize;
      i++;
    }
  
  data_output.y          = xmodel;
  data_output.signal     = signal;
  data_output.signal_err = signal_err;
  data_output.model      = OIDataM2V(xmodel, oi_parameters_output.amplitude, oi_parameters_output.offset, oi_parameters_output.centre, oi_parameters_output.sigma,ips.sex,0.0,ips.m2_general_y);

};

fit_parameters2 OICoordInitial(vector<vector<double> > coords, vector<double> signal)
{
  fit_parameters2 initial_values;
  vector<double> dx, dy;
  //split apart coords to calculate dx0 and dy0 seperately
  
  for (vector<vector<double> >::iterator i = coords.begin(); i != coords.end(); ++i)
    {
      dx.push_back((*i)[0]);
      dy.push_back((*i)[1]);
    }

  //double mean_dx   = std::accumulate(dx.begin(), dx.end(), 0.0) / dx.size();
  //double mean_dy   = std::accumulate(dy.begin(), dy.end(), 0.0) / dy.size();

  //this method of choosing the best x and y initial is based on the fact that
  //the data always has horizontal scan data THEN vertical data
  //a cludge I know... but it gives the best result provided they're in this order
  //relative to any other algorithm
  
  //double mean_dx   = dx.back();

  double mean_dx   = WeightedMean(dx,signal);

  double mean_dy   = dy[0];
  double amplitude = *max_element(signal.begin(), signal.end());
  double offset    = *min_element(signal.begin(), signal.end());
  
  initial_values.dx0.first       = mean_dx;
  initial_values.dy0.first       = mean_dy;
  initial_values.sex.first       = 110.0;
  initial_values.sey.first       = 1.5;
  initial_values.amplitude.first = amplitude;
  initial_values.offset.first    = offset;

  return initial_values;
}


void FitOICoordEmpirical (vector<vector<double> > coords, vector<double> signal, vector<double> signal_err)
{
  #if PRINTOUT
  std::cout<<"Fitting to Overlap Integral - Empirical model"<<std::endl;
  std::cout<<"Using combined horizontal & vertical"<<std::endl;
  //calculate initial parameters
  std::cout << "Calculating initial values" << std::endl;
  #endif
  fit_parameters2 initial = OICoordInitial(coords, signal);

  #if PRINTOUT
  std::cout<<"Initial DX_0       "<<initial.dx0.first       <<std::endl;
  std::cout<<"Initial DY_0       "<<initial.dy0.first       <<std::endl;
  std::cout<<"Initial Sigma X    "<<initial.sex.first       <<std::endl;
  std::cout<<"Initial Sigma Y    "<<initial.sey.first       <<std::endl;
  std::cout<<"Initial Amplitude: "<<initial.amplitude.first <<std::endl;
  std::cout<<"Initial Offset:    "<<initial.offset.first    <<std::endl;
  #endif
    
  //construct the function
  OICoordEmpiricalFcn theFCN(coords, signal, signal_err);

  //construct initial parameters
  //par[0] = sex
  //par[1] = sey
  //par[2] = dx0
  //par[3] = dy0
  //par[4] = offset
  //par[5] = amplitude
  
  MnUserParameters upar;
  upar.add("sex", initial.sex.first,10.0);
  upar.add("sey", initial.sey.first,0.2);
  upar.add("dx0", initial.dx0.first,2.0);
  upar.add("dy0", initial.dy0.first,0.2);
  upar.add("offset", initial.offset.first, 0.1);  
  upar.add("amplitude", initial.amplitude.first, 0.1);
  
 
  //set parameter limits
  upar.setLimits("sex", 90.0, 200.0);
  upar.setLimits("sey", 0.8, 15.0);
  upar.setLimits("amplitude", 0.01,20.0);
  
  //construct MIGRAD minimiser
  MnMigrad migrad(theFCN, upar);
  
  //minimise
  std::cout << "Minimising..." << std::endl;
  #if PRINTOUT
  std::cout << "RedChi2\t\tSigma_X\tSigma_Y\tDX0\tDY0\tOffset\tAmplitude"<<std::endl;
  #endif
  FunctionMinimum min = migrad();
  if (!min.isValid())
  {
    //try with higher strategy
    std::cout<<"Invalid minimum - trying with higher strategy"<<std::endl;
    MnMigrad migrad2(theFCN, upar, 2);
    min = migrad2();
    if (!min.isValid())
      {
	std::cout<<"No valid minimum found"<<std::endl;
      }
  }
  std::cout << "Finished minimisation" << std::endl;

  //estimate errors using MINOS
  std::cout << "Estimating uncertainties using Minos" << std::endl;
  MnMinos minos(theFCN, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;
  #if PRINTOUT
  std::cout << "Sigma_{ex} uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_sex = minos(0,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Sigma_{ey} uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_sey = minos(1,minosmaxcalls);
  #if PRINTOUT
  std::cout << "DX0 uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_dx0 = minos(2,minosmaxcalls);
  #if PRINTOUT
  std::cout << "DY0 uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_dy0 = minos(3,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Offset uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_off = minos(4,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Amplitude uncertainty:" << std::endl;
  #endif
  std::pair<double,double> e_amp = minos(5,minosmaxcalls);
  #if PRINTOUT
  std::cout << "Finished uncertainties:" << std::endl;
  #endif

  double rchi2 = min.fval() / (coords.size()-6.0-1.0);

  //report the final fit values
  #if PRINTOUT
  std::cout << "Fitted values are:\t"<<std::endl;
  std::cout << "Reduced Chi2:\t"<< rchi2<<std::endl;
  std::cout << "sigma_ex:\t"    << min.userState().value("sex")       << "\t+" << e_sex.second << "\t" << e_sex.first << std::endl;
  std::cout << "sigma_ey:\t"    << min.userState().value("sey")       << "\t+" << e_sey.second << "\t" << e_sey.first << std::endl;
  std::cout << "dx0:\t\t"       << min.userState().value("dx0")       << "\t+" << e_dx0.second << "\t" << e_dx0.first << std::endl;
  std::cout << "dy0:\t\t"       << min.userState().value("dy0")       << "\t+" << e_dy0.second << "\t" << e_dy0.first << std::endl;
  std::cout << "offset:\t\t"    << min.userState().value("offset")    << "\t+" << e_off.second << "\t" << e_off.first << std::endl;
  std::cout << "amplitude:\t"   << min.userState().value("amplitude") << "\t+" << e_amp.second << "\t" << e_amp.first << std::endl;
  #endif
  
  //export the final values along with the asymmetric uncertainties
  oi_parameters_output2.sex       = std::make_pair(min.userState().value("sex"), e_sex );
  oi_parameters_output2.sey       = std::make_pair(min.userState().value("sey"), e_sey );
  oi_parameters_output2.dx0       = std::make_pair(min.userState().value("dx0"), e_dx0 );
  oi_parameters_output2.dy0       = std::make_pair(min.userState().value("dy0"), e_dy0 );
  oi_parameters_output2.offset    = std::make_pair(min.userState().value("offset"), e_off );
  oi_parameters_output2.amplitude = std::make_pair(min.userState().value("amplitude"), e_amp );
  oi_parameters_output2.metric    = rchi2;
  
  //generate interpolated model data (for nice smooth curve)
  //remember main list of coords is x scan followed by y scan and each covers different ranges
  //so interpolate each separately, then put together with the central value from the fit
  //for the other dimension

  //prepare variables
  double max_x, min_x, max_y, min_y, stepsize_x, stepsize_y, pos_x, pos_y;
  int npoints = MODEL_NPOINTS;
  int i = 0;
  vector<double> xcoords, ycoords, xcoords_model, ycoords_model, x_repeats, y_repeats, signal_model;
  vector<vector<double> > coords_model;

  //split main coords list into two vectors
  for (vector<vector<double > >::iterator i = coords.begin(); i != coords.end(); ++i)
    {
      xcoords.push_back((*i)[0]);
      ycoords.push_back((*i)[1]);
    }

  //determine the range in each and calculate the interpolated points
  //generate points for other axis too when doing this
  //use the fitted centres dx0, dy0 for padding coords
  max_x = *max_element(xcoords.begin(), xcoords.end() );
  min_x = *min_element(xcoords.begin(), xcoords.end() );
  max_y = *max_element(ycoords.begin(), ycoords.end() );
  min_y = *min_element(ycoords.begin(), ycoords.end() );
  stepsize_x = (max_x - min_x) / double(npoints);
  stepsize_y = (max_y - min_y) / double(npoints);
  pos_x = min_x;
  pos_y = min_y;
  while (i < npoints)
    {
      xcoords_model.push_back(pos_x);
      ycoords_model.push_back(pos_y);
      pos_x += stepsize_x;
      pos_y += stepsize_y;
      x_repeats.push_back(oi_parameters_output2.dx0.first);
      y_repeats.push_back(oi_parameters_output2.dy0.first);
      i++;
    }

  //put xcoords in output data struct
  data_output2.x = xcoords;
  data_output2.y = ycoords;

  //need to reserve memory before joining two vectors
  data_output2.x_model.reserve(xcoords_model.size() + x_repeats.size() );
  data_output2.y_model.reserve(ycoords_model.size() + y_repeats.size() );
  //build final list of coordinates  
  //horizontal, then vertical scans in combined coordinates
  data_output2.x_model.insert(data_output2.x_model.end(), xcoords_model.begin(), xcoords_model.end() );
  data_output2.x_model.insert(data_output2.x_model.end(), x_repeats.begin(), x_repeats.end() );
  data_output2.y_model.insert(data_output2.y_model.end(), y_repeats.begin(), y_repeats.end() );
  data_output2.y_model.insert(data_output2.y_model.end(), ycoords_model.begin(), ycoords_model.end() );
  
  //put original signal in so output file is standalone
  data_output2.signal     = signal;
  data_output2.signal_err = signal_err;

  //zip model coordinates together to supply to OI function
  vector<double> tempholder;
  for (int i = 0; i < data_output2.x_model.size(); i++)
    {
      tempholder.push_back(data_output2.x_model[i]);
      tempholder.push_back(data_output2.y_model[i]);
      coords_model.push_back(tempholder);
      tempholder.clear();
    }

  //calculate model overlap integrals and put in structure
  data_output2.signal_model = OIDataCoordE (coords_model, 
					    oi_parameters_output2.sex.first,
					    oi_parameters_output2.sey.first,
					    oi_parameters_output2.dx0.first, 
					    oi_parameters_output2.dy0.first, 
					    oi_parameters_output2.offset.first, 
					    oi_parameters_output2.amplitude.first );
};


void FitOICoordEmpiricalNoErrors (vector<vector<double> > coords, vector<double> signal, vector<double> signal_err)
{
  #if PRINTOUT
  std::cout<<"Fitting to Overlap Integral - Empirical model"<<std::endl<<std::endl;
  std::cout<<"Using combined horizontal & vertical"<<std::endl<<std::endl;
  //calculate initial parameters
  std::cout << "Calculating initial values" << std::endl;
  #endif
  fit_parameters2 initial = OICoordInitial(coords, signal);

  #if PRINTOUT
  std::cout<<"Initial DX_0       "<<initial.dx0.first       <<std::endl;
  std::cout<<"Initial DY_0       "<<initial.dy0.first       <<std::endl;
  std::cout<<"Initial Sigma X    "<<initial.sex.first       <<std::endl;
  std::cout<<"Initial Sigma Y    "<<initial.sey.first       <<std::endl;
  std::cout<<"Initial Amplitude: "<<initial.amplitude.first <<std::endl;
  std::cout<<"Initial Offset:    "<<initial.offset.first    <<std::endl;
  #endif
    
  //construct the function
  OICoordEmpiricalFcn theFCN(coords, signal, signal_err);

  //construct initial parameters
  //par[0] = sex
  //par[1] = sey
  //par[2] = dx0
  //par[3] = dy0
  //par[4] = offset
  //par[5] = amplitude
  
  MnUserParameters upar;
  upar.add("sex", initial.sex.first,2.0);
  upar.add("sey", initial.sey.first,0.1);
  upar.add("dx0", initial.dx0.first,1.0);
  upar.add("dy0", initial.dy0.first,0.2);
  upar.add("offset", initial.offset.first, 0.01);  
  upar.add("amplitude", initial.amplitude.first, 0.2);
  
 
  //set parameter limits
  upar.setLimits("sex", 10.0, 500.0);
  upar.setLimits("sey", 0.2, 30.0);
  upar.setLimits("amplitude", 0.00001,20000.0);
  
  //construct MIGRAD minimiser
  MnMigrad migrad(theFCN, upar);
  
  //minimise
  std::cout << "Starting minimisation" << std::endl;
  #if PRINTOUT
  std::cout << "RedChi2\t\tSigma_X\tSigma_Y\tDX0\tDY0\tOffset\tAmplitude"<<std::endl;
  #endif
  FunctionMinimum min = migrad();
  std::cout << "Finished minimisation" << std::endl;

  //dummy errors
  std::pair<double,double> e_sex, e_sey, e_dx0, e_dy0, e_off, e_amp;
  e_sex = std::make_pair(0,0);
  e_sey = std::make_pair(0,0);
  e_dx0 = std::make_pair(0,0);
  e_dy0 = std::make_pair(0,0);
  e_off = std::make_pair(0,0);
  e_amp = std::make_pair(0,0);

  double rchi2 = min.fval() / (coords.size()-6.0-1.0);

  //report the final fit values
  #if PRINTOUT
  std::cout << "Fitted values are:\t"<<std::endl;
  std::cout << "Reduced Chi2:\t"<< rchi2<<std::endl;
  std::cout << "sigma_ex:\t"    << min.userState().value("sex")       << "\t+" << e_sex.second << "\t" << e_sex.first << std::endl;
  std::cout << "sigma_ey:\t"    << min.userState().value("sey")       << "\t+" << e_sey.second << "\t" << e_sey.first << std::endl;
  std::cout << "dx0:\t\t"       << min.userState().value("dx0")       << "\t+" << e_dx0.second << "\t" << e_dx0.first << std::endl;
  std::cout << "dy0:\t\t"       << min.userState().value("dy0")       << "\t+" << e_dy0.second << "\t" << e_dy0.first << std::endl;
  std::cout << "offset:\t\t"    << min.userState().value("offset")    << "\t+" << e_off.second << "\t" << e_off.first << std::endl;
  std::cout << "amplitude:\t"   << min.userState().value("amplitude") << "\t+" << e_amp.second << "\t" << e_amp.first << std::endl;
  #endif
  
  //export the final values along with the asymmetric uncertainties
  oi_parameters_output2.sex       = std::make_pair(min.userState().value("sex"), e_sex );
  oi_parameters_output2.sey       = std::make_pair(min.userState().value("sey"), e_sey );
  oi_parameters_output2.dx0       = std::make_pair(min.userState().value("dx0"), e_dx0 );
  oi_parameters_output2.dy0       = std::make_pair(min.userState().value("dy0"), e_dy0 );
  oi_parameters_output2.offset    = std::make_pair(min.userState().value("offset"), e_off );
  oi_parameters_output2.amplitude = std::make_pair(min.userState().value("amplitude"), e_amp );
  oi_parameters_output2.metric    = rchi2;
};


