#ifndef FIT_H_
#define FIT_H_

#include "config.hh"
#include "mystructs.hh"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <math.h>
#include <numeric>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>

#include "fifo.hh"
#include <gsl/gsl_integration.h>
#include "oi.hh"

#include "Minuit/FunctionMinimum.h"
#include "Minuit/MnUserParameterState.h"
#include "Minuit/MnMigrad.h"
#include "Minuit/MnMinos.h"
#include "Minuit/MnContours.h"
#include "Minuit/MnPlot.h"
#include "Minuit/MnStrategy.h"
#include "Minuit/MnUserParameters.h"
#include "Minuit/MnConfig.h"
#include "fcn.hh"

#define  MODEL_NPOINTS 2000;



using std::vector;

extern input_parameters ips;

extern fit_data        data_input, data_output;
extern fit_data2       data_output2;
extern fit_parameters  oi_parameters_output;
extern fit_parameters3 oi_parameters_output2;
extern fit_parameters4 oi_parameters_output3;

double          WeightedMean        (vector<double> x, vector<double> weights);
fit_parameters  GaussInitial        (vector<double> y, vector<double> signal);
void            FitGaussV           (vector<double> y, vector<double> signal, vector<double> signal_err);
void            FitOIEmpiricalV     (vector<double> y, vector<double> signal, vector<double> signal_err);
void            FitOIM2V            (vector<double> y, vector<double> signal, vector<double> signal_err); 
void            FitOIEmpiricalH     (vector<double> x, vector<double> signal, vector<double> signal_err); 
void            FitOIM2H            (vector<double> x, vector<double> signal, vector<double> signal_err); 
fit_parameters2 OICoordInitial      (vector<vector<double> > coords, vector<double> signal);
void            FitOICoordEmpirical (vector<vector<double> > coords, vector<double> signal, vector<double> signal_err);
void            FitOICoordEmpiricalNoErrors (vector<vector<double> > coords, vector<double> signal, vector<double> signal_err);

#endif /*FIT_H_*/
