#include "generate.hh"

vector<vector<double> > GenerateCoords (double xmin, double xmax, double ymin, double ymax, int nx, int ny)
{
  vector<double> xcoords, ycoords, temppair;
  vector<vector<double> > coords;
  double xstep, ystep;
  
  xstep = (xmax - xmin)/double(nx);
  ystep = (ymax - ymin)/double(ny);

  for (double xpos = xmin; xpos < xmax+0.0001; xpos+=xstep)
    {
      xcoords.push_back(xpos);
    }

  for (double ypos = ymin; ypos < ymax+0.0001; ypos+=ystep)
    {
      ycoords.push_back(ypos);
    }

  for (vector<double>::iterator i = xcoords.begin(); i != xcoords.end(); ++i)
    {
      for (vector<double>::iterator j = ycoords.begin(); j != ycoords.end(); ++j)
	{
	  temppair.push_back(*i);
	  temppair.push_back(*j);
	  coords.push_back(temppair);
	  temppair.clear();
	}
    }
  return coords;
}

void GenerateSurfaceData(fit_data2 data_input2, fit_parameters3 oi_parameters_output2)
{
  std::cout << "Generating OI surface data" << std::endl;
  
  //declarations
  //gets surface_data sd from main.cc
  double xmin,xmax,ymin,ymax,sex,sey,dx0,dy0,offset,amplitude;

  //calculate and get all the parameters
  xmin      = *min_element(data_input2.x.begin(), data_input2.x.end());
  xmax      = *max_element(data_input2.x.begin(), data_input2.x.end());
  ymin      = *min_element(data_input2.y.begin(), data_input2.y.end());
  ymax      = *max_element(data_input2.y.begin(), data_input2.y.end());
  sex       = oi_parameters_output2.sex.first;
  sey       = oi_parameters_output2.sey.first;
  dx0       = oi_parameters_output2.dx0.first;
  dy0       = oi_parameters_output2.dy0.first;
  offset    = oi_parameters_output2.offset.first;
  amplitude = oi_parameters_output2.amplitude.first;
  
  //generate the coordinates
  int nspoints = SURFACE_NPOINTS;
  
  vector<vector<double> > coords = GenerateCoords(xmin,xmax,ymin,ymax,nspoints,nspoints);
  
  //calculate the OIs
  sd.s = OIDataCoordE(coords,sex,sey,dx0,dy0,offset,amplitude);
  #if PRINTOUT
  std::cout << "Finished generating surface data" << std::endl;
  #endif
  
  //fill in data structure
  for (vector<vector<double> >::iterator i = coords.begin(); i != coords.end(); ++i)
    {
      sd.x.push_back((*i)[0]);
      sd.y.push_back((*i)[1]);
    }
}

/*

int main()
{
  double xmin,xmax,ymin,ymax;
  xmin = -1500.0;
  xmax = 1500.0;
  ymin = -100.0;
  ymax = 100.0;
  
  vector<vector<double> >results;
  results = GenerateCoords(xmin,xmax,ymin,ymax,20,20);
  
  for (vector<vector<double> >::iterator i = results.begin(); i != results.end(); ++i)
    {
      for (vector<double>::iterator j = (*i).begin(); j != (*i).end(); ++j)
	{
	  std::cout<<*j<<" ";
	}
      std::cout<<std::endl;
    }

  //GenerateSurfaceData();
  //for (int i = 0; i < sd.x.size(); ++i)
  // {
  //  std::cout << sd.x[i] << "\t" << sd.y[i] << "\t" << sd.x[i] << std::endl;
  //}
}
*/
