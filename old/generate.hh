#ifndef GENERATE_H_
#define GENERATE_H_

#include "config.hh"
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <vector>
#include "oi.hh"
#include "mystructs.hh"
#include "fifo.hh"

#define SURFACE_NPOINTS 400;

using std::vector;

//extern fit_data2       data_input2;
//extern fit_parameters3 oi_parameters_output2;
extern surface_data    sd;

vector<vector<double> > GenerateCoords (double xmin, double xmax, double ymin, double ymax, int nx, int ny);
void GenerateSurfaceData (fit_data2 data_input2, fit_parameters3 oi_parameters_output2);



#endif /* GENERATE_H_ */
