//Laser-wire Overlap Integral Fitting Program
//Version:  4
//Date:     2013_05_01
//Author:   L Nevay

#include "config.hh"

#include <cstdlib>
#include "fit.hh"
#include <iostream>
#include "oi.hh"
#include <string>
#include <vector>
#include "fifo.hh"
#include <gsl/gsl_integration.h>
#include "mystructs.hh"
#include "generate.hh"


fit_data        data_input;
fit_data        data_output;
fit_data2       data_output2;
fit_parameters  oi_parameters_output;
fit_parameters3 oi_parameters_output2;
fit_parameters4 oi_parameters_output3;
surface_data    sd;

int main (int argc, char* argv[])
{
  if (argc != 2)
    {
      std::cout << "Too few parameters "<< std::endl;
      std::cout << "Usage: "<< std::endl;
      std::cout << " (E)  - Empirical Laser Model" << std::endl;
      std::cout << " (M2) - Single M2 Laser Model" << std::endl;
      std::cout << "   -a - Single OI empirical laser propagation"<< std::endl;
      std::cout << "   -b - Single OI M2 laser propagation"<< std::endl;
      std::cout << "   -c - OI (E)  set for various y values about (0,0)" << std::endl;
      std::cout << "   -d - OI (E)  set for various x values about (0,0)" << std::endl;
      std::cout << "   -e - OI (E)  set normalised for various chamber y values" << std::endl;
      std::cout << "   -f - OI (E)  set normalised for various chamber x values" << std::endl;
      std::cout << "   -g - OI (M2) set for various y values about (0,0)" << std::endl;
      std::cout << "   -h - OI (M2) set for various x values about (0,0)" << std::endl;
      std::cout << "   -i - OI (M2) set normalised for various chamber y values" << std::endl;
      std::cout << "   -j - OI (M2) set normalised for various chamber x values" << std::endl;
      std::cout << "   -k - Fit OI with empirical laser propagation" << std::endl;
      std::cout << "   -l - Fit OI with M2 laser propagation" << std::endl;
      std::cout << "   -m - Fit OI with Gaussian" << std::endl;
      std::cout << "   -n - Fit OI with empirical laser propagation in the HORIZONTAL" << std::endl;
      std::cout << "   -o - Fit OI with M2 laser propagation in the HORIZONTAL" << std::endl;
      std::cout << "   -p - Fit OI for combined Horizontal & Vertical with Emprical Laser Propagation" << std::endl;
      std::cout << "   -q - Like -p but additionally generates OIs for surface given fitted params" << std::endl;
      exit(0);
    }
  else {
    OIInitialise();
    if (std::string(argv[1]) == "-a") 
      {
	std::cout << "Single OI Empirical Laser Propagation"<< std::endl;
	double result;
	Fifo a;
	a.ReadSingleOI(std::string("../singleoi_input.dat"));
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(100000);
	result = OIEmpirical(w,a.sv.dy,a.sv.dx,a.sv.sex,a.sv.sey);
	gsl_integration_workspace_free (w);
	std::cout << "Result = " <<result<<std::endl;
	a.WriteSingleOI(result, std::string("../singleoi_output.dat"));
      } 
    else if (std::string(argv[1]) == "-b") 
      {
	std::cout << "Single OI M2 Laser Propagation"<< std::endl;
	double result;
	Fifo a;
	a.ReadSingleOI(std::string("../singleoi_input.dat"));
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(100000);
	result = OIM2_y(w,a.sv.dy,a.sv.dx,a.sv.sex,a.sv.sey,a.sv.m2);
	gsl_integration_workspace_free (w);
	std::cout << "Result = " <<result<<std::endl;
	a.WriteSingleOI(result, std::string("../singleoi_output.dat"));
      }
    else if (std::string(argv[1]) == "-c")
      {
	std::cout << "OI (E) set for various y values about (0,0)" << std::endl;
	vector<double> results;
	Fifo a;
	a.ReadSingleVectorOI(std::string("../singleoivector_input.dat"));
	results = OIDataEmpiricalRawV(a.sv.x,a.sv.sex,a.sv.sey,a.sv.dx);
	a.WriteSingleVectorOI(results, std::string("../singleoivector_output.dat"));
      }
    else if (std::string(argv[1]) == "-d")
      {
	std::cout << "OI (E) set for various x values about (0,0)" << std::endl;
	vector<double> results;
	Fifo a;
	a.ReadSingleVectorOI(std::string("../singleoivector_input.dat"));
	results = OIDataEmpiricalRawH (a.sv.x,a.sv.sex,a.sv.sey,a.sv.dy);
	a.WriteSingleVectorOI(results, std::string("../singleoivector_output.dat"));
      }
    else if (std::string(argv[1]) == "-e")
      {
	std::cout << "OI (E) set Normalised for various chamber y values" << std::endl;
	vector<double> results;
	Fifo a;
	a.ReadSingleVectorOI(std::string("../singleoivector_input.dat"));
	results = OIDataEmpiricalV (a.sv.x,a.sv.amplitude,a.sv.offset,a.sv.centre,a.sv.sex,a.sv.sey,a.sv.dx);
	a.WriteSingleVectorOI(results, std::string("../singleoivector_output.dat"));
      }  
    else if (std::string(argv[1]) == "-f")
      {
	std::cout << "OI (E) set Normalised for various chamber x values" << std::endl;
	vector<double> results;
	Fifo a;
	a.ReadSingleVectorOI(std::string("../singleoivector_input.dat"));
	results = OIDataEmpiricalH (a.sv.x,a.sv.amplitude,a.sv.offset,a.sv.centre,a.sv.sex,a.sv.sey,a.sv.dy);
	a.WriteSingleVectorOI(results, std::string("../singleoivector_output.dat"));
      }  
    else if (std::string(argv[1]) == "-g")
      {
	std::cout << "OI (M2) set for various y values about (0,0)" << std::endl;
	vector<double> results;
	Fifo a;
	a.ReadSingleVectorOI(std::string("../singleoivector_input.dat"));
	results = OIDataM2RawV(a.sv.x,a.sv.sex,a.sv.sey,a.sv.dx,a.sv.m2);
	a.WriteSingleVectorOI(results, std::string("../singleoivector_output.dat"));
      }
    else if (std::string(argv[1]) == "-h")
      {
	std::cout << "OI (M2) set for various x values about (0,0)" << std::endl;
	vector<double> results;
	Fifo a;
	a.ReadSingleVectorOI(std::string("../singleoivector_input.dat"));
	results = OIDataM2RawH (a.sv.x,a.sv.sex,a.sv.sey,a.sv.dy,a.sv.m2);
	a.WriteSingleVectorOI(results, std::string("../singleoivector_output.dat"));
      }
    else if (std::string(argv[1]) == "-i")
      {
	std::cout << "OI (M2) set Normalised for various chamber y values" << std::endl;
	vector<double> results;
	Fifo a;
	a.ReadSingleVectorOI(std::string("../singleoivector_input.dat"));
	results = OIDataM2V (a.sv.x,a.sv.amplitude,a.sv.offset,a.sv.centre,a.sv.sex,a.sv.sey,a.sv.dx,a.sv.m2);
	a.WriteSingleVectorOI(results, std::string("../singleoivector_output.dat"));
      }  
    else if (std::string(argv[1]) == "-j")
      {
	std::cout << "OI (M2) set Normalised for various chamber x values" << std::endl;
	vector<double> results;
	Fifo a;
	a.ReadSingleVectorOI(std::string("../singleoivector_input.dat"));
	results = OIDataM2H (a.sv.x,a.sv.amplitude,a.sv.offset,a.sv.centre,a.sv.sex,a.sv.sey,a.sv.dy,a.sv.m2);
	a.WriteSingleVectorOI(results, std::string("../singleoivector_output.dat"));
      }  
    else if (std::string(argv[1]) == "-k")
      {
	std::cout << "Fit OI with empirical laser propagation" << std::endl;
	Fifo f_fifo;
	f_fifo.ReadInputData("../fit_data_input.dat");
	FitOIEmpiricalV(f_fifo.data_input.y,f_fifo.data_input.signal,f_fifo.data_input.signal_err);
	f_fifo.WriteOutputData(data_output,"../fit_data_output.dat");
	f_fifo.WriteFitParameters3(oi_parameters_output3,"../fit_parameters_output.dat");
      }
    else if (std::string(argv[1]) == "-l")
      {
	std::cout << "Fit OI with M2 laser propagation" << std::endl;
	Fifo f_fifo;
	f_fifo.ReadInputData("../fit_data_input.dat");
	FitOIM2V(f_fifo.data_input.y,f_fifo.data_input.signal,f_fifo.data_input.signal_err);
	f_fifo.WriteOutputData(data_output,"../fit_data_output.dat");
	f_fifo.WriteFitParameters3(oi_parameters_output3,"../fit_parameters_output.dat");
      }
    else if (std::string(argv[1]) == "-m")
      {
	std::cout << "Fit OI with Gaussian" << std::endl;
	Fifo f_fifo;
	f_fifo.ReadInputData("../fit_data_input.dat");
	FitGaussV(f_fifo.data_input.y,f_fifo.data_input.signal,f_fifo.data_input.signal_err);
	f_fifo.WriteOutputData(data_output,"../fit_data_output.dat");
	f_fifo.WriteFitParameters(oi_parameters_output,"../fit_parameters_output.dat");
      }
    else if (std::string(argv[1]) == "-n")
      {
	std::cout << "Fit OI with empirical laser propgation in the HORIZONTAL" << std::endl;
	Fifo f_fifo;
	f_fifo.ReadInputData("../fit_data_input.dat");
	FitOIEmpiricalH(f_fifo.data_input.y,f_fifo.data_input.signal,f_fifo.data_input.signal_err);
	f_fifo.WriteOutputData(data_output,"../fit_data_output.dat");
	f_fifo.WriteFitParameters3(oi_parameters_output3,"../fit_parameters_output.dat");
      }
    else if (std::string(argv[1]) == "-o")
      {
	#if PRINTOUT
	std::cout << "Fit OI with M2 laser propgation in the HORIZONTAL" << std::endl;
	#endif
	Fifo f_fifo;
	f_fifo.ReadInputData("../fit_data_input.dat");
	//FitGaussV(f_fifo.data_input.y,f_fifo.data_input.signal,f_fifo.data_input.signal_err);
	//f_fifo.WriteOutputData(data_output,"../fit_data_output.dat");
	//f_fifo.WriteFitParameters(oi_parameters_output,"../fit_parameters_output.dat");
      }
    else if (std::string(argv[1]) == "-p")
      {
	#if PRINTOUT
	std::cout << "Fit OI for combined Horizontal & Vertical with Emprical Laser Propagation" << std::endl;
	#endif
	Fifo f_fifo;
	f_fifo.ReadInputData2("../fit_data_input.dat");
	FitOICoordEmpirical(f_fifo.coords_input,f_fifo.data_input2.signal,f_fifo.data_input2.signal_err);
	f_fifo.WriteOutputData2(data_output2,"../fit_data_output.dat");
	f_fifo.WriteFitParameters2(oi_parameters_output2,"../fit_parameters_output.dat");
      }
     else if (std::string(argv[1]) == "-q")
      {
	#if PRINTOUT
	std::cout << "Fit OI for combined Horizontal & Vertical with Emprical Laser Propagation" << std::endl;
	std::cout << "And generate OIs to represent fitted surface" << std::endl;
	#endif
	Fifo f_fifo;
	f_fifo.ReadInputData2("../fit_data_input.dat");
	FitOICoordEmpirical(f_fifo.coords_input,f_fifo.data_input2.signal,f_fifo.data_input2.signal_err);
	f_fifo.WriteOutputData2(data_output2,"../fit_data_output.dat");
	f_fifo.WriteFitParameters2(oi_parameters_output2,"../fit_parameters_output.dat");
	GenerateSurfaceData(f_fifo.data_input2,oi_parameters_output2);
	f_fifo.WriteSurfaceData(sd,"../fit_data_output_surface.dat");	
      }
    else if (std::string(argv[1]) == "-r")
      {
	#if PRINTOUT
	std::cout << "Fit OI for combined Horizontal & Vertical with Emprical Laser Propagation" << std::endl;
	std::cout << "Without errorbars for speed" << std::endl;
	#endif
	Fifo f_fifo;
	f_fifo.ReadInputData2("../fit_data_input.dat");
	FitOICoordEmpiricalNoErrors(f_fifo.coords_input,f_fifo.data_input2.signal,f_fifo.data_input2.signal_err);
	f_fifo.WriteFitParameters2(oi_parameters_output2,"../fit_parameters_output.dat");
      }

    else {
      std::cout << "Invalid option" << std::endl;
      std::cout << "Usage: "<< std::endl;
      std::cout << " (E)  - Empirical Laser Model" << std::endl;
      std::cout << " (M2) - Single M2 Laser Model" << std::endl;
      std::cout << "   -a - Single OI empirical laser propagation"<< std::endl;
      std::cout << "   -b - Single OI M2 laser propagation"<< std::endl;
      std::cout << "   -c - OI (E)  set for various y values about (0,0)" << std::endl;
      std::cout << "   -d - OI (E)  set for various x values about (0,0)" << std::endl;
      std::cout << "   -e - OI (E)  set normalised for various chamber y values" << std::endl;
      std::cout << "   -f - OI (E)  set normalised for various chamber x values" << std::endl;
      std::cout << "   -g - OI (M2) set for various y values about (0,0)" << std::endl;
      std::cout << "   -h - OI (M2) set for various x values about (0,0)" << std::endl;
      std::cout << "   -i - OI (M2) set normalised for various chamber y values" << std::endl;
      std::cout << "   -j - OI (M2) set normalised for various chamber x values" << std::endl;
      std::cout << "   -k - Fit OI with empirical laser propagation" << std::endl;
      std::cout << "   -l - Fit OI with M2 laser propagation" << std::endl;
      std::cout << "   -m - Fit OI with Gaussian" << std::endl;
      std::cout << "   -n - Fit OI with empirical laser propgation in the HORIZONTAL" << std::endl;
      std::cout << "   -o - Fit OI with M2 laser propagation in the HORIZONTAL" << std::endl;
      std::cout << "   -p - Fit OI for combined Horizontal & Vertical with Emprical Laser Propagation" << std::endl;
      std::cout << "   -q - Like -p but additionally generates OIs for surface given fitted params" << std::endl;
      exit(0);
    }
  }
  return 0;
}
