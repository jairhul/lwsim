#ifndef MYSTRUCTS_H_
#define MYSTRUCTS_H_

#include "config.hh"
#include <vector>
#include <utility>

using std::vector;

struct func_parameters {
  double dx;
  double dy;
  double sex;
  double sey;
  double m2;
};

struct single_params {
  vector<double> x;
  double dx;
  double dy;
  double sex;
  double sey;
  double m2;
  double amplitude;
  double offset;
  double centre;
};

struct fit_parameters {
  double centre;
  double offset;
  double sigma;
  double amplitude;
  double metric;
  double centre_err;
  double offset_err;
  double sigma_err;
  double amplitude_err;
};

struct fit_parameters2 {
  std::pair<double, double> sex;
  std::pair<double, double> sey;
  std::pair<double, double> dx0;
  std::pair<double, double> dy0;
  std::pair<double, double> offset;
  std::pair<double, double> amplitude;
  double metric;
};

struct fit_parameters3 {
  std::pair<double, std::pair<double, double> > sex;
  std::pair<double, std::pair<double, double> > sey;
  std::pair<double, std::pair<double, double> > dx0;
  std::pair<double, std::pair<double, double> > dy0;
  std::pair<double, std::pair<double, double> > offset;
  std::pair<double, std::pair<double, double> > amplitude;
  double metric;
};

struct fit_parameters4 {
  std::pair<double, std::pair<double, double> > centre;
  std::pair<double, std::pair<double, double> > offset;
  std::pair<double, std::pair<double, double> > amplitude;
  std::pair<double, std::pair<double, double> > sigma;
  double  metric;
};


struct fit_data {
  vector<double> y;
  vector<double> signal;
  vector<double> signal_err;
  vector<double> model;
};

struct fit_data2 {
  vector<double> x;
  vector<double> y;
  vector<double> signal;
  vector<double> signal_err;
  vector<double> x_model;
  vector<double> y_model;
  vector<double> signal_model;
};

struct surface_data {
  vector<double> x;
  vector<double> y;
  vector<double> s;
};

struct input_parameters {
  double m2_z;
  double m2_y;
  double z_o_z;
  double z_o_y;
  double sigma_o_z;
  double sigma_o_y;
  double theta;
  double m2_general_y;
  double wavelength;
  double sex;
  double dx;
};

#endif /*MYSTRUCTS_H_*/
