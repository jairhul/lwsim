//Laser-wire Overlap Integral
//Version:  2
//Date:     2013_02_17
//Author:   L Nevay

#include "oi.hh"

input_parameters ips;

void OIInitialise ()
{
  //read laser parameters from static file
  Fifo a;
  a.ReadParameters("../fit_parameters_input.dat");
  //a.PrintStructs();
  ips = a.ips;
}

double OIGaussian (double dy, double sey)
{
  //0.998um is the simulated best performance of lens
  double sig_l, sig_m, p1, p2, p3, res;
  sig_l = ips.m2_y * 0.998;
  sig_m = sqrt( (pow(sey,2) + pow(sig_l,2)) );
  p1    = 1.0/sqrt(2*pi);
  p2    = 1.0/sig_m;
  p3    = -0.5*pow((dy/sig_m),2);
  res   = p1*p2*exp(p3);
  return res;
}

double OIEmpirical (gsl_integration_workspace* w, double dy, double dx, double sex, double sey)
{
  double result, error;
  func_parameters ps;

  ps.dx  = dx;
  ps.dy  = dy;
  ps.sex = sex;
  ps.sey = sey;
    
  gsl_function F;
  F.function = &F_Empirical;
  F.params   = &ps;

  gsl_integration_qagi (&F, 0, 1e-7, 100000, w, &result, &error);
  //gsl_integration_workspace_free (w);

  return result;
}

double OIM2_y (gsl_integration_workspace* w, double dy, double dx, double sex, double sey, double m2_y)
{
  //equation A5 in Agapov 2007 with vertical M2 number
  double result, error;
  func_parameters ps;

  ps.dx  = dx;
  ps.dy  = dy;
  ps.sex = sex;
  ps.sey = sey;
  ps.m2  = m2_y;
  
  gsl_function F;
  F.function = &F_M2;
  F.params   = &ps;

  gsl_integration_qagi (&F, 0, 1e-7, 100000, w, &result, &error);
  
  return result;
}

double F_Empirical (double x, void *params)
{
  //equation A2 in Agapov 2007 with Sig_s from laser measurement
  func_parameters *p  = (func_parameters *) params;
  double dx, dy, sex, sey, sig_l, sig_s, p1, p2, p3, p4, res;
  
  dx  = p->dx;
  dy  = p->dy;
  sey = p->sey;
  sex = p->sex;
  
  sig_l  = Sigl(x,dx);
  sig_s  = sqrt( (pow(sey,2)+pow(sig_l,2)) );

  p1  = 1.0/(2*pi*sex);
  p2  = 1.0/sig_s;
  p3  = -0.5*pow((x/sex),2);
  p4  = -0.5*pow((dy/sig_s),2);
  
  res = p1*p2*exp(p3+p4);
  return res;
}

double F_M2 ( double x, void *params)
{
  func_parameters *p  = (func_parameters *) params;
  double dx, dy, sex, sey, m2, sig_o, fr, sig_s, r,vx;
  dx  = p->dx;
  dy  = p->dy;
  sey = p->sey;
  sex = p->sex;
  m2  = p->m2;

  sig_o = m2*0.998;
  //0.998um is theoretical best performance of lens at 532nm

  fr    = Fr(x,dx,m2,sig_o);
  sig_s = sqrt( pow(sey,2.0) + (fr*pow(sig_o,2.0) ));
  r     = (1.0/(2*pi*sex))*(1.0/sig_s)*exp( -0.5*(pow((x/sex),2.0)) - 0.5*(pow((dy/sig_s),2.0)));
  return r;
}

double Fr ( double x, double dx, double m2, double sig_o )
{
  double res, wavelength_si, xr_si, sig_o_si;

  wavelength_si = ips.wavelength*1e-6;
  sig_o_si      = sig_o*1e-6;
  
  //calculate rayleigh range in SI units
  xr_si = Zr(sig_o_si,m2,wavelength_si);

  //overlap integral is in microns so SI *1e6
  res = 1 + pow( ((x-dx)/(xr_si*1e6)),2 );
  return res;
}

double Sigl (double x, double dx)
{
  double angle_rad, vx, hx, vsig, hsig, psig;
  angle_rad = (ips.theta/180.0)*pi;
  
  vx        = ips.z_o_y + x - dx;
  hx        = ips.z_o_z + x - dx;

  vsig      = M2Sigma(vx,ips.m2_y,ips.z_o_y,ips.sigma_o_y,ips.wavelength);
  hsig      = M2Sigma(hx,ips.m2_z,ips.z_o_z,ips.sigma_o_z,ips.wavelength);

  psig      = sqrt( pow((hsig*sin(angle_rad)),2) + pow((vsig*cos(angle_rad)),2) );

  return psig;
}

double M2Sigma(double z, double m2, double zo, double sig_o, double wavelength)
{
  //sigma generated for arbitrary axis with m2 parameters
  //follows m2 definitions of using z for optical axis
  //takes input units of microns and converts to SI
  //returns in microns
  
  double z_r_si, sigma, sigma_si, z_si, zo_si, sig_o_si, wavelength_si;
  z_si          = z*1e-6;
  zo_si         = zo*1e-6;
  sig_o_si      = sig_o*1e-6;
  wavelength_si = wavelength*1e-6;
  
  z_r_si        = Zr(sig_o_si,m2,wavelength_si);
  sigma_si      = sig_o_si*sqrt( (1+ pow(((z_si-zo_si)/z_r_si),2)) );
  sigma         = sigma_si*1e6;

  //std::cout << "Rayleigh Range (SI): " << z_r_si << std::endl;
  //std::cout << "Calculated Sigma (um): " << sigma << std::endl;
  return sigma;
}

double Zr(double sig_o, double m2, double wavelength)
{
  //rayleigh range in SI units
  double zr;
  zr  = (pi*pow((2*sig_o),2)) / (m2*wavelength);
  return zr;
}

vector<double> GaussianDataV (vector<double> y, double amplitude, double offset, double centre, double sey)
{
  //generate data assuming the laser has a Gaussian profile in y and that the
  //laser sigma does not vary with x - ie infinite rayleigh range
  vector<double> result;
  for (vector<double>::iterator i = y.begin(); i != y.end(); ++i)
    {
      double dy, r;
      dy  = *i - centre;
      r   = (amplitude*OIGaussian(dy,sey)) + offset;
      result.push_back(r);
    };
  return result;
}

vector<double> OIDataEmpiricalV (vector<double> y, double amplitude, double offset, double centre, double sex, double sey, double dx)
{
  //generate overlap integral data with appropriate normalisation
  //use empirical laser information
  vector<double> ois, result, y_minus_c;
  double diff, m, r;
  for (vector<double>::iterator i = y.begin(); i != y.end(); ++i)
    {
      diff = *i - centre;
      y_minus_c.push_back(*i-centre);
    }
  ois = OIDataEmpiricalRawV(y_minus_c,sex,sey,dx);
  m = *max_element(ois.begin(),ois.end());
  //can't normalise by zero - if it's super low, set to 1
  if (m < 1e-6)
    {m = 1.0;}
  for (vector<double>::iterator i = ois.begin(); i != ois.end(); ++i)
    {
      r = offset + amplitude*((*i)/m);
      result.push_back(r);
    }
  return result;
};

vector<double> OIDataEmpiricalH (vector<double> x, double amplitude, double offset, double centre, double sex, double sey, double dy)
{
  //generate overlap integral data with appropriate normalisation
  //use empirical laser information
  vector<double> ois, result, x_minus_c;
  double m, r;
  for (vector<double>::iterator i = x.begin(); i != x.end(); ++i)
    {
      x_minus_c.push_back(*i-centre);
    }
  ois = OIDataEmpiricalRawH(x_minus_c,sex,sey,dy);
  m = *max_element(ois.begin(),ois.end());
  //can't normalise by zero - if it's super low, set to 1
  if (m < 1e-6)
    {m = 1.0;}
  for (vector<double>::iterator i = ois.begin(); i != ois.end(); ++i)
    {
      r = offset + amplitude*((*i)/m);
      result.push_back(r);
    }
  return result;
};

vector<double> OIDataEmpiricalRawV (vector<double> y, double sex, double sey, double dx)
{
  //generate overlap integral data WITHOUT normalisation
  //use empirical laser information
  vector<double> result;
  double dy, overlap_integral;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc (100000);
  for (vector<double>::iterator i = y.begin(); i != y.end(); ++i)
    {
      dy = *i;
      overlap_integral = OIEmpirical(w, dy, dx, sex, sey);
      //std::cout << *i << "\t" << overlap_integral << std::endl;
      result.push_back(overlap_integral);
    };
  gsl_integration_workspace_free (w);
  return result;
};

vector<double> OIDataEmpiricalRawH (vector<double> x, double sex, double sey, double dy)
{
  //generate overlap integral data WITHOUT normalisation
  //use empirical laser information
  vector<double> result;
  double dx, overlap_integral;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc (100000);
  for (vector<double>::iterator i = x.begin(); i != x.end(); ++i)
    {
      dx = *i;
      overlap_integral = OIEmpirical(w, dy, dx, sex, sey);
      result.push_back(overlap_integral);
    };
  gsl_integration_workspace_free (w);
  return result;
};

vector<double> OIDataM2V (vector<double> y, double amplitude, double offset, double centre, double sex, double sey, double dx, double m2_y)
{
  //generate overlap integral data with appropriate normalisation
  //use m2 in y dimension only
  vector<double> ois, result, y_minus_c;
  double dy, overlap_integral, m, r;
  for (vector<double>::iterator i = y.begin(); i != y.end(); ++i)
    {
      y_minus_c.push_back(*i-centre);
    }
  ois = OIDataM2RawV(y_minus_c,sex,sey,dx,m2_y);
  m = *max_element(ois.begin(),ois.end());
  //can't normalise by zero - if it's super low, set to 1
  if (m < 1e-6)
    {m = 1.0;}
  for (vector<double>::iterator i = ois.begin(); i != ois.end(); ++i)
    {
      r = offset + amplitude*((*i)/m);
      result.push_back(r);
    }
  return result;
}

vector<double> OIDataM2H (vector<double> x, double amplitude, double offset, double centre, double sex, double sey, double dy, double m2_y)
{
  //generate overlap integral data with appropriate normalisation
  //use m2 in y dimension only
  vector<double> ois, result, x_minus_c;
  double m, r;
  for (vector<double>::iterator i = x.begin(); i != x.end(); ++i)
    {
      x_minus_c.push_back(*i-centre);
    }
  ois = OIDataM2RawH(x_minus_c,sex,sey,dy,m2_y);
  m = *max_element(ois.begin(),ois.end());
  //can't normalise by zero - if it's super low, set to 1
  if (m < 1e-6)
    {m = 1.0;}
  for (vector<double>::iterator i = ois.begin(); i != ois.end(); ++i)
    {
      r = offset + amplitude*((*i)/m);
      result.push_back(r);
    }
  return result;
}

vector<double> OIDataM2RawV (vector<double> y, double sex, double sey, double dx, double m2_y)
{
  //generate overlap integral data WITHOUT normalisation
  //use m2 in y dimension only
  vector<double> result;
  double dy, overlap_integral;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc (100000);
  
  for (vector<double>::iterator i = y.begin(); i != y.end(); ++i)
    {
      dy      = *i;
      overlap_integral = OIM2_y(w, dy, dx, sex, sey, m2_y);
      result.push_back(overlap_integral);
    };
  gsl_integration_workspace_free (w);
  return result;
}

vector<double> OIDataM2RawH (vector<double> x, double sex, double sey, double dy, double m2_y)
{
  //generate overlap integral data WITHOUT normalisation
  //use m2 in y dimension only
  vector<double> result;
  double dx, overlap_integral;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc (100000);
  
  for (vector<double>::iterator i = x.begin(); i != x.end(); ++i)
    {
      dx      = *i;
      overlap_integral = OIM2_y(w, dy, dx, sex, sey, m2_y);
      result.push_back(overlap_integral);
    };
  gsl_integration_workspace_free (w);
  return result;
}

vector<double> OIDataCoordsM (vector<vector<double> > coords,  double sex, double sey, double dx0, double dy0, double offset, double amplitude, double msq)
{
  vector<double> result, ois, temppair;
  vector<vector<double> > tempcoords;
  double overlap_integral, m;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc (100000);

  for (vector<vector<double> >::iterator i = coords.begin(); i != coords.end(); ++i)
    {
      temppair.push_back((*i)[0]-dx0);
      temppair.push_back((*i)[1]-dy0);
      tempcoords.push_back(temppair);
      temppair.clear();
    }
  
  //do the dirty - calculate the ois
  for (vector<vector<double> >::iterator i = coords.begin(); i != coords.end(); ++i)
    {
      //OIEmpirical(w,dy,dx,sex,sey,msq)
      overlap_integral = OIM2_y(w,(*i)[1],(*i)[0],sex,sey,msq);
      ois.push_back(overlap_integral);
    }
  gsl_integration_workspace_free (w);

  //work out normalisation
  m = *max_element(ois.begin(),ois.end());
  if (m < 1e-6)
    {m = 1.0;}
  //can't normalise by zero - if it's super low, set to 1
  
  //normalise, multiply by amplitude and add offset
  for (vector<double>::iterator i = ois.begin(); i != ois.end(); ++i)
    {
      result.push_back( offset + amplitude*((*i)/m));
    }
  return result;
}

vector<double> OIDataCoordE (vector<vector<double> > coords,  double sex, double sey, double dx0, double dy0, double offset, double amplitude)
{
  vector<double> result, ois, temppair;
  vector<vector<double> > tempcoords;
  double overlap_integral, m;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc (100000);

  for (vector<vector<double> >::iterator i = coords.begin(); i != coords.end(); ++i)
    {
      temppair.push_back((*i)[0]-dx0);
      temppair.push_back((*i)[1]-dy0);
      tempcoords.push_back(temppair);
      temppair.clear();
      //std::cout<<(*i)[0]<<" "<<dx0<<" "<<(*i)[0]-dx0<<std::endl;
    }
  
  //do the dirty - calculate the ois
  for (vector<vector<double> >::iterator i = tempcoords.begin(); i != tempcoords.end(); ++i)
    {
      //OIEmpirical(w,dy,dx,sex,sey)
      overlap_integral = OIEmpirical(w,(*i)[1],(*i)[0],sex,sey);
      ois.push_back(overlap_integral);
    }
  gsl_integration_workspace_free (w);

  //work out normalisation
  m = *max_element(ois.begin(),ois.end());
  if (m < 1e-6)
    {m = 1.0;}
  //can't normalise by zero - if it's super low, set to 1
  
  //normalise, multiply by amplitude and add offset
  for (vector<double>::iterator i = ois.begin(); i != ois.end(); ++i)
    {
      result.push_back( offset + amplitude*((*i)/m));
    }
  return result;
}



/*
int main ()
{
  OIInitialise ();
  vector<double> y_values, results;
  Fifo b;
  fit_data inp, res;
  b.ReadInputData("../fit_data_input.dat");
  inp = b.data_input;
  y_values  = inp.y;
  res.y     = inp.y;
  
  //NOTE: these input parameters should be adjusted to match the input data
  results= OIDataEmpiricalV (y_values, 3.4, 1.21, 850, 235.0, 1.1, 0.0);
  for (vector<double>::iterator i = results.begin(); i != results.end(); ++i)
    {
      std::cout << *i  << "\t";
    }
  std::cout<<std::endl;

  //do it 100 times to guage speed
  for (int i = 0; i < 100; i++)
    {
      std::cout<<i<<std::endl;
      results = OIDataEmpiricalV (y_values, 3.4, 1.21, 850.0, 235.0, 1.1, 0.0);
    }
  //print out results
  std::cout << "Results is " << results.size() << " elements long" <<  std::endl << "Results: " << std::endl;
  for (vector<double>::iterator i = results.begin(); i != results.end(); ++i)
    {
      std::cout << *i << "\t";
    }
  std::cout << std::endl << std::endl;
  
  //write to file
  //res.signal     = inp.signal;
  //res.signal_err = inp.signal_err;
  //res.model      = results;

  //b.WriteOutputData(res, "../fit_data_output.dat");
  gsl_integration_workspace *w = gsl_integration_workspace_alloc (100000);
  double samp_val = OIEmpirical(w,-99.9019,0.0,235.5,16.008);
  std::cout<<"Sample value: "<<samp_val<<std::endl;
  return 0;
}
*/

