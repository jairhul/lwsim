#ifndef OI_H_
#define OI_H_

#include "config.hh"
#include "mystructs.hh"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <cstdlib>
#include <math.h>
#include <algorithm>
#include <stdio.h>
#include <gsl/gsl_integration.h>
#include "fifo.hh"

using std::vector;
using std::max_element;

const double pi = 3.14159265359;

void OIInitialise();
double OIGaussian (double dy, double sey);
double OIEmpirical (gsl_integration_workspace* w, double dy, double dx, double sex, double sey);
double OIM2_y (gsl_integration_workspace* w, double dy, double dx, double sex, double sey, double m2_y);
double F_Empirical (double x, void *params);
double F_M2 (double x, void *params);
double Fr (double x, double dx, double m2, double sig_o);
double Sigl (double x, double dx);
double M2Sigma(double z, double m2, double zo, double sig_o, double wavelength);
double Zr(double sig_o, double m2, double wavelength);
vector<double> GaussianDataV (vector<double> y, double amplitude, double offset, double centre, double sey);
vector<double> OIDataEmpiricalV (vector<double> y, double amplitude, double offset, double centre, double sex, double sey, double dx);
vector<double> OIDataEmpiricalH (vector<double> x, double amplitude, double offset, double centre, double sex, double sey, double dy);
vector<double> OIDataEmpiricalRawV (vector<double> y, double sex, double sey, double dx);
vector<double> OIDataEmpiricalRawH (vector<double> x, double sex, double sey, double dy);
vector<double> OIDataM2V (vector<double> y, double amplitude, double offset, double centre, double sex, double sey, double dx, double m2_y);
vector<double> OIDataM2H (vector<double> x, double amplitude, double offset, double centre, double sex, double sey, double dy, double m2_y);
vector<double> OIDataM2RawV (vector<double> y, double sex, double sey, double dx, double m2_y);
vector<double> OIDataM2RawH (vector<double> x, double sex, double sey, double dy, double m2_y);
vector<double> OIDataCoordE (vector<vector<double> > coords, double sex, double sey, double dx0, double dy0, double offset, double amplitude);
vector<double> OIDataCoordM (vector<vector<double> > coords, double sex, double sey, double dx0, double dy0, double offset, double amplitude, double msq);



#endif /*OI_H_*/

