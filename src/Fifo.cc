#include "Fifo.hh"
#include "Point.hh"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

void LWS::WriteScanToFile(std::string                fileName,
			  const std::vector<double>& x,
			  std::string                xColumnName,
			  const std::vector<double>& y,
			  std::string                yColumnName)
{
  std::ofstream file;
  file.open(fileName, std::fstream::out);

  file << xColumnName << "\t" << yColumnName << "\n";

  file << std::left << std::setprecision(8) << std::scientific;
  int nPoints = (x.end()--) - x.begin();
  for (int i = 0; i < nPoints; i++)
    {file << x[i] << "\t" << y[i] << "\n";}
  
  file.close();
}

void LWS::WriteScan2DToFile(std::string              fileName,
			    const std::vector<LWS::point>& data)
{
  std::ofstream file;
  file.open(fileName, std::fstream::out);

  file << "# X\tY\tValue\n";

  file << std::left << std::setprecision(8) << std::scientific;

  for (const auto& point : data)
    {file << point.x << "\t" << point.y << "\t" << point.value << "\n";}
  
  file.close();
}
  
void LWS::WriteVector(std::string                fileName,
		      const std::vector<double>& v)
{
  std::ofstream file;
  file.open(fileName, std::fstream::out);

  file << std::left << std::setprecision(8) << std::scientific;

  for (const auto& val : v)
    {file << val << "\n";}
  
  file.close();
}
