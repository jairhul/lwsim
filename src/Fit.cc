#include "Fit.hh"
#include "FitFunction.hh"
#include "FitParameters.hh"

#include "root6/Minuit2/FunctionMinimum.h"
#include "root6/Minuit2/MnMigrad.h"
#include "root6/Minuit2/MnMinos.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <utility>
#include <vector>

#define MINOSMAXCALLS 1000

double LWS::WeightedMean(const std::vector<double>& x,
			 const std::vector<double>& weights)
{
  int sz_x, sz_w;
  double min, nom, denom, r;
  sz_x = (int)x.size();
  sz_w = (int)weights.size();

  if (sz_x != sz_w)
    {
      int middle = sz_x/2;
      r = middle;
    }
  else 
    {
      nom   = 0;
      denom = 0;
      for (int i = 0; i < sz_x; i++)
	{
	  nom   += (x[i]*weights[i]);
	  denom += weights[i];
	}
      r = nom / denom;
    }
  return r;
}

LWS::FitParameters LWS::GaussianInitial(const std::vector<double>& x,
					const std::vector<double>& signal)
{
  double max_x, min_x, max_s, min_s;
  double amplitude, offset, centre, sigma;
  max_x = *std::max_element(x.begin(), x.end());
  min_x = *std::min_element(x.begin(), x.end());
  max_s = *std::max_element(signal.begin(), signal.end());
  min_s = *std::min_element(signal.begin(), signal.end());

  amplitude = max_s - min_s;
  offset    = min_s;
  std::vector<double> s_high_power;
  for (auto v : signal)
    {s_high_power.push_back(std::pow(v,3));}
  
  centre = LWS::WeightedMean(x, s_high_power);
  sigma  = (max_x - min_x)*0.08;

  LWS::FitParameters initial = {centre, offset, sigma, amplitude, 1.0};
  initial.limitsSet = true;
  initial.limitCentreLow  = min_x;
  initial.limitCentreHigh = max_x;
  initial.limitOffsetLow  = min_s - 0.1*amplitude;
  initial.limitOffsetHigh = max_s;
  initial.limitSigmaLow   = std::abs(x[1]-x[0]);
  initial.limitSigmaHigh  = std::abs(max_x - min_x);
  initial.limitAmplitudeLow  = std::abs(0.01 * amplitude);
  initial.limitAmplitudeHigh = std::abs(100 * amplitude);

  return initial;
};

LWS::FitParameters LWS::FitGaussian(const std::vector<double>& x,
				    const std::vector<double>& signal,
				    const std::vector<double>& signal_err,
				    bool verbose)
{
  LWS::FitParameters initial = GaussianInitial(x, signal);
  
  LWS::FcnGaussian function(x, signal, signal_err, verbose);

  ROOT::Minuit2::MnUserParameters upar;
  upar.Add("amplitude", initial.amplitude, 0.1);
  upar.Add("offset",    initial.offset,    0.1);
  upar.Add("centre",    initial.centre,    0.1);
  upar.Add("sigma",     initial.sigma,     0.1);
  if (initial.limitsSet)
    {
      upar.SetLimits("amplitude", initial.limitAmplitudeLow, initial.limitAmplitudeHigh);
      upar.SetLimits("offset", initial.limitOffsetLow, initial.limitOffsetHigh);
      upar.SetLimits("centre", initial.limitCentreLow, initial.limitCentreHigh);
      upar.SetLimits("sigma", initial.limitSigmaLow, initial.limitSigmaHigh);
    }
  
  ROOT::Minuit2::MnMigrad migrad(function, upar);
  if (verbose)
    {
      std::cout << "Minimising" << std::endl;
      function.PrintVerboseHeader();
    }
  ROOT::Minuit2::FunctionMinimum min = migrad();
  double rchi2 = min.Fval() / (x.size()-4.0-1.0);
  ROOT::Minuit2::MnMinos minos(function, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;

  std::pair<double,double> e_amp = minos(0,minosmaxcalls);
  std::pair<double,double> e_off = minos(1,minosmaxcalls);
  std::pair<double,double> e_cen = minos(2,minosmaxcalls);
  std::pair<double,double> e_sey = minos(3,minosmaxcalls); 

  LWS::FitParameters result(initial); // copy all values first - copies limits
  result.amplitude = min.UserState().Value("amplitude");
  result.offset    = min.UserState().Value("offset");
  result.centre    = min.UserState().Value("centre");
  result.sigma     = min.UserState().Value("sigma");
  result.metric    = rchi2;

  result.amplitude_err = std::max(e_amp.first,e_amp.second);
  result.offset_err    = std::max(e_off.first,e_off.second);
  result.centre_err    = std::max(e_cen.first,e_cen.second);
  result.sigma_err     = std::max(e_sey.first,e_sey.second);
  return result;
}

LWS::FitParameters LWS::FitVerticalScanEmpirical(const LWS::Laser* laser,
						 double sigmaX,
						 const std::vector<double>& y,
						 const std::vector<double>& signal,
						 const std::vector<double>& signalError,
						 bool verbose)
{
  LWS::FitParameters initialGuess = LWS::FitGaussian(y, signal, signalError, verbose);

  LWS::FcnOIEV function = LWS::FcnOIEV(laser, sigmaX, y, signal, signalError, verbose);

  ROOT::Minuit2::MnUserParameters upar;
  upar.Add("amplitude", initialGuess.amplitude, 0.2);
  upar.Add("offset",    initialGuess.offset,    0.01);
  upar.Add("centre",    initialGuess.centre,    1.0);
  upar.Add("sigma",     initialGuess.sigma,     0.1);
  if (initialGuess.limitsSet)
    {
      upar.SetLimits("amplitude", initialGuess.limitAmplitudeLow, initialGuess.limitAmplitudeHigh);
      upar.SetLimits("offset", initialGuess.limitOffsetLow, initialGuess.limitOffsetHigh);
      upar.SetLimits("centre", initialGuess.limitCentreLow, initialGuess.limitCentreHigh);
      upar.SetLimits("sigma", initialGuess.limitSigmaLow, initialGuess.limitSigmaHigh);
    }
  
  ROOT::Minuit2::MnMigrad migrad(function, upar);
  ROOT::Minuit2::FunctionMinimum min = migrad();
  double rchi2 = min.Fval() / (y.size()-4.0-1.0);

  ROOT::Minuit2::MnMinos minos(function, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;
  std::pair<double,double> e_amp = minos(0,minosmaxcalls);
  std::pair<double,double> e_off = minos(1,minosmaxcalls);
  std::pair<double,double> e_cen = minos(2,minosmaxcalls);
  std::pair<double,double> e_sey = minos(3,minosmaxcalls);

  LWS::FitParameters result;
  result.amplitude = min.UserState().Value("amplitude");
  result.offset    = min.UserState().Value("offset");
  result.centre    = min.UserState().Value("centre");
  result.sigma     = min.UserState().Value("sigma");
  result.metric    = rchi2;

  result.amplitude_err = std::max(e_amp.first,e_amp.second);
  result.offset_err    = std::max(e_off.first,e_off.second);
  result.centre_err    = std::max(e_cen.first,e_cen.second);
  result.sigma_err     = std::max(e_sey.first,e_sey.second);
  return result;
}

LWS::FitParameters FitHorizontalScanEmpirical(const LWS::Laser* laser,
					      double sigmaY,
					      const std::vector<double>& x,
					      const std::vector<double>& signal,
					      const std::vector<double>& signalError,
					      bool verbose)
{
  LWS::FitParameters initial = LWS::GaussianInitial(x, signal);
  initial.sigma = initial.sigma;

  //construct the function
  LWS::FcnOIEH function(laser, sigmaY, x, signal, signalError, verbose);
  ROOT::Minuit2::MnUserParameters upar;
  upar.Add("amplitude", initial.amplitude, 0.2);
  upar.Add("offset", initial.offset, 0.01);
  upar.Add("centre", initial.centre, 1.0);
  upar.Add("sigma", initial.sigma, 0.1);
  upar.SetLimits("sigma", 20, 1000.0);
  upar.SetLimits("amplitude", 0.01,20000.0);
  
  ROOT::Minuit2::MnMigrad migrad(function, upar);
  ROOT::Minuit2::FunctionMinimum min = migrad();
  double rchi2 = min.Fval() / (x.size()-4.0-1.0);
  
  ROOT::Minuit2::MnMinos minos(function, min);
  unsigned int minosmaxcalls = MINOSMAXCALLS;  
  std::pair<double,double> e_amp = minos(0,minosmaxcalls);
  std::pair<double,double> e_off = minos(1,minosmaxcalls);
  std::pair<double,double> e_cen = minos(2,minosmaxcalls);
  std::pair<double,double> e_sex = minos(3,minosmaxcalls);

  LWS::FitParameters result;
  result.amplitude = min.UserState().Value("amplitude");
  result.offset    = min.UserState().Value("offset");
  result.centre    = min.UserState().Value("centre");
  result.sigma     = min.UserState().Value("sigma");
  result.metric    = rchi2;
  result.amplitude_err = std::max(e_amp.first,e_amp.second);
  result.offset_err    = std::max(e_off.first,e_off.second);
  result.centre_err    = std::max(e_cen.first,e_cen.second);
  result.sigma_err     = std::max(e_sex.first,e_sex.second);
  return result;
}
