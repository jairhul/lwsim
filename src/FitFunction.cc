#include "FitFunction.hh"
#include "LaserwireScan.hh"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>

double LWS::FcnB::RedChi2(const std::vector<double>& model) const
{
  double chi2 = 0.;
  int nmeas = theMeasurements.size();
  double nmeas_d = theMeasurements.size();
  for (int n = 0; n < nmeas; n++)
    {chi2 += ((model[n]-theMeasurements[n])/theMstds[n])*((model[n]-theMeasurements[n])/theMstds[n]);}
  double redchi2 = chi2 / (nmeas_d-4.0-1.0);
  return chi2;
}

std::vector<double> LWS::Gaussian(const std::vector<double> x,
				  double amplitude,
				  double offset,
				  double centre,
				  double sigmaY)
{
  auto Gauss = [&](double xv){return offset + amplitude * std::exp(-std::pow((xv-centre),2)/(2*std::pow(sigmaY,2)));};

  std::vector<double> result;
  for (auto xi : x)
    {result.push_back(Gauss(xi));}
  return result;
}

double LWS::FcnGaussian::operator()(const std::vector<double>& par) const
{
  assert(par.size() == 4);

  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey
  if (verbose)
    {PrintParameters(par);}
  std::vector<double> model = LWS::Gaussian(thePositions, par[0], par[1], par[2], par[3]);
  double redchi2 = RedChi2(model);
  if (verbose)
    {std::cout << redchi2 << std::endl;}
  return redchi2;
}

double LWS::FcnOIEV::operator() (const std::vector<double>& par) const
{
  assert(par.size() == 4);
  
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sey

  if (verbose)
    {PrintParameters(par);}
  //std::vector<double> model = OIDataEmpiricalV(thePositions, par[0], par[1], par[2], sex, par[3], dx);
  std::vector<double> posMinusOffset;
  for (auto v : thePositions)
    {posMinusOffset.push_back(v-par[2]);}
  std::vector<double> model = LWS::VerticalScanOI(laser,
						  sigmaX,
						  par[3],
						  posMinusOffset);

  double m = *std::max_element(model.begin(), model.end());
  //can't normalise by zero - if it's super low, set to 1
  if (m < 1e-6)
    {m = 1.0;}
  for (auto& v : model)
    {v = par[1] + par[0]*(v/m);}

  double redchi2 = RedChi2(model);
  if (verbose)
    {std::cout << redchi2 << std::endl;}
  return redchi2;
}

double LWS::FcnOIEH::operator()(const std::vector<double>& par) const
{
  assert(par.size() == 4);
  
  //par[0] = amplitude
  //par[1] = offset
  //par[2] = centre
  //par[3] = sex
  if (verbose)
    {PrintParameters(par);}

  std::vector<double> posMinusOffset;
  for (auto v : thePositions)
    {posMinusOffset.push_back(v-par[2]);}
  std::vector<double> model = LWS::HorizontalScanOI(laser, par[3], sigmaY, posMinusOffset);

  double m = *std::max_element(model.begin(), model.end());
  //can't normalise by zero - if it's super low, set to 1
  if (m < 1e-6)
    {m = 1.0;}
  for (auto& v : model)
    {v = par[1] + par[0]*(v/m);}

  double redchi2 = RedChi2(model);
  if (verbose)
    {std::cout << redchi2 << std::endl;}
  return redchi2;
}