#include "Laser.hh"
#include "Utilities.hh"

#include <cmath>

LWS::Laser::Laser(double wavelengthIn,
		  double sigmaTIn,
		  double pulseEnergyIn,
		  double thetaIn):
  wavelength(wavelengthIn),
  sigmaT(sigmaTIn),
  pulseEnergy(pulseEnergyIn),
  theta(thetaIn),
  peakPower(pulseEnergyIn / (2 * std::sqrt(2*std::log((double)2)) * sigmaTIn))
{// note std::log is log_e - this is the conversion to FWHM
  rotated = LWS::IsFinite(theta);
}

double LWS::Laser::WX(const double& z) const
{
  if (!rotated)
    {return WXUnrotated(z);}
  else
    {
      double wx = WXUnrotated(z);
      double wy = WYUnrotated(z);
      // here we assume the +ve root
      double wxr = std::sqrt(std::pow((wx * std::cos(theta)), 2) + std::pow((wy * std::sin(theta)), 2));
      return wxr;
    }
}

double LWS::Laser::WY(const double& z) const
{
  if (!rotated)
    {return WYUnrotated(z);}
  else
    {
      double wx = WXUnrotated(z);
      double wy = WYUnrotated(z);
      // here we assume the +ve root
      double wyr = std::sqrt(std::pow((wx * std::sin(theta)), 2) + std::pow((wy * std::cos(theta)), 2));
      return wyr;
    }
}

double LWS::Laser::SigmaX(const double& z) const
{return 0.5 * WX(z);}

double LWS::Laser::SigmaY(const double& z) const
{return 0.5 * WX(z);}

double LWS::Laser::SigmaXUnrotated(const double& z) const
{return 0.5 * WXUnrotated(z);}

double LWS::Laser::SigmaYUnrotated(const double& z) const
{return 0.5 * WXUnrotated(z);}

void LWS::Laser::SigmaXY(const double& z,
			 double&       sigmaX,
			 double&       sigmaY) const
{
  sigmaX = SigmaX(z);
  sigmaY = SigmaY(z);
}
