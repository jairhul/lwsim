#include "LaserGaussian.hh"
#include "Utilities.hh"

#include <cmath>

LWS::LaserGaussian::LaserGaussian():
  LaserGaussian(532e-9, 2e-6, 1.0, 2e-6, 1.0)
{;}

LWS::LaserGaussian::LaserGaussian(double wavelengthIn,
				  double w0In,
				  double m2In,
				  double sigmaTIn,
				  double pulseEnergyIn):
  LaserGaussian(wavelengthIn, w0In, m2In, w0In, m2In, 0, 0, sigmaTIn, pulseEnergyIn, 0)
{;}

LWS::LaserGaussian::LaserGaussian(double wavelengthIn,
				  double w0xIn, double m2xIn,
				  double w0yIn, double m2yIn,
				  double z0xIn, double z0yIn,
				  double sigmaTIn,
				  double pulseEnergyIn,
				  double thetaIn):
  Laser(wavelengthIn, sigmaTIn, pulseEnergyIn, thetaIn),
  w0x(w0xIn), w0y(w0yIn),
  m2x(m2xIn), m2y(m2yIn),
  z0x(z0xIn), z0y(z0yIn)
{
  rrx = LWS::RayleighRange(wavelength, w0x, m2x);
  rry = LWS::RayleighRange(wavelength, w0y, m2y);
}

LWS::LaserGaussian::~LaserGaussian()
{;}

double LWS::LaserGaussian::WXUnrotated(const double& z) const
{
  // should always be positive - no need to catch exceptions here
  return w0x * std::sqrt(1.0 + std::pow( (( z - z0x)/rrx), 2));
}

double LWS::LaserGaussian::WYUnrotated(const double& z) const
{
  // should always be positive - no need to catch exceptions here
  return w0y * std::sqrt(1.0 + std::pow( (( z - z0y)/rry), 2));
}
