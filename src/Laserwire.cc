#include "Constants.hh"
#include "Laser.hh"
#include "Laserwire.hh"
#include "Structures.hh"

#include <gsl/gsl_integration.h>

#include <cmath>
#include <stdexcept>

double LWS::MaximumComptonPhotonEnergy(const Laser* laser,
                                       double electronEnergy)
{
  double v0    = LWS::c_light / laser->Wavelength();
  double gamma = LWS::ElectronLorentzGammaFromEnergy(electronEnergy);
  double e1    = gamma * LWS::h * v0 / (LWS::electron_mass * LWS::c_light_squared);
  double hvmaxInGeV = 2*electronEnergy*e1 / (1+2*e1);
  return hvmaxInGeV;
}

double LWS::ElectronLorentzGammaFromEnergy(const double energy)
{
  return energy / electron_mass_gev;
}

double LWS::ComptonThomsonRatio(const Laser* laser,
				const double electronEnergy)
{
  double v0    = LWS::c_light / laser->Wavelength();
  double gamma = ElectronLorentzGammaFromEnergy(electronEnergy);
  double e1    = gamma * LWS::h * v0 / (LWS::electron_mass * LWS::c_light_squared);
  
  double p1 = ((1 + e1) / std::pow(e1,3)) * ( 2*e1 * (1 + e1) / (1 + 2*e1) - std::log(1 + 2*e1));
  double p2 = (1. / (2*e1)) * std::log(1 + 2*e1);
  double p3 = (1 + 3*e1) / std::pow((1 + 2*e1), 2);

  double ratio = 0.75 * (p1 + p2 - p3);
  return ratio;
}

double LWS::ComptonCrossSection(const Laser* laser,
				const double electronEnergy)
{
  double ratio     = LWS::ComptonThomsonRatio(laser, electronEnergy);
  double comptonCS = ratio * LWS::thomson_cross_section;
  
  return comptonCS;
}

double LWS::PhotodetachmentCrossSection(const Laser* laser,
                                        const double ionEnergy)
{
  /*
  const double fitCoefficient1 = 2.79945909e-12;
  const double fitCoefficient2 = 1.24982046e-8;
  const double fitCoefficient3 = 1.00207573e-5;
  const double fitCoefficient4 = 3.64205996e-3;
  const double fitCoefficient5 = 7.30751085e-2;
*/
  double photonEnergy = (LWS::h*LWS::c_light)/laser->Wavelength();
  double photonLorentz = photonEnergy*(ionEnergy/LWS::hydrogen_ion_mass_gev);
  double photonEnergyJoules = photonLorentz*(1/LWS::joules_gev); // TBC - isn't used

  double relativisticGamma = (ionEnergy/LWS::hydrogen_ion_mass_gev);
  double photon_shift = relativisticGamma*((photonEnergy)/LWS::electron_charge);
  double lambda_shift = LWS::h*LWS::c_light/(photon_shift*LWS::electron_charge);
  double lambda_nm = lambda_shift * 1e9;
  double first = 2.79945909e-12*pow(lambda_nm,4.0);
  double second = 1.24982046e-8*pow(lambda_nm,3.0);
  double third = 1.00207573e-5*pow(lambda_nm,2.0);
  double fourth = 3.64205996e-3*lambda_nm;
  double fifth = 7.30751085e-2;
  
  double l_cross_fact = first-second+third+fourth-fifth;
  double photodetachmentCrossSection = l_cross_fact*1e-17*1e-6;

  return photodetachmentCrossSection;
}

double LWS::OIGaussian(const Laser*  laser,
		       const double& sigma_ey,
		       const double& dy)
{
  double sig_l, sig_m, dyum, p1, p2, p3, res;
  sig_l = laser->SigmaY(0) * 1e6;
  sig_m = std::sqrt( (std::pow(sigma_ey * 1e6, 2) + std::pow(sig_l, 2)) );
  p1    = 1.0/std::sqrt(LWS::twopi);
  p2    = 1.0/sig_m;
  dyum  = dy * 1e6; // move to microns
  p3    = -0.5 * std::pow((dyum/sig_m),2);
  res   = p1*p2*exp(p3);
  return res;
}

double LWS::OIGaussianSI(const Laser*  laser,
                       const double& sigma_ey,
                       const double& dy)
{
  double sig_l, sig_m, dyum, p1, p2, p3, res;
  sig_l = laser->SigmaY(0);
  sig_m = std::sqrt( (std::pow(sigma_ey, 2) + std::pow(sig_l, 2)) );
  p1    = 1.0/std::sqrt(LWS::twopi);
  p2    = 1.0/sig_m;
  dyum  = dy;
  p3    = -0.5 * std::pow((dyum/sig_m),2);
  res   = p1*p2*exp(p3);
  return res;
}

double LWS::OI(const Laser* laser,
	       const double& sigma_ex,
	       const double& sigma_ey,
	       const double& dx,
	       const double& dy,
	       gsl_integration_workspace* w,
	       double epsabs,
	       double epsrel)
{
  if (!laser)
    {throw std::invalid_argument("Invalid laser");}
  
  double result, error;
  LWS::func_parameters ps;

  ps.dx    = dx;
  ps.dy    = dy;
  ps.sex   = sigma_ex;
  ps.sey   = sigma_ey;
  ps.laser = laser;
    
  gsl_function F;
  F.function = &LWS::OIFunction;
  F.params   = &ps;

  bool localWorkSpace = false;
  if (!w)
    {
      w = gsl_integration_workspace_alloc(100000);
      localWorkSpace = true;
    }

  gsl_integration_qagi(&F, epsabs, epsrel, 100000, w, &result, &error);

  if (localWorkSpace)
    {gsl_integration_workspace_free(w);}

  return result;
}

double LWS::OIFunction(double x, void* params)
{
  //equation A2 in Agapov 2007 with Sig_s from laser measurement

  // cast structure to correct type
  LWS::func_parameters* p = static_cast<LWS::func_parameters*>(params);
  
  double dx, dy, sex, sey, sig_l, sig_s, p1, p2, p3, p4, res;

  const Laser* las = p->laser;
  
  dx  = p->dx  * 1e6; // move to microns
  dy  = p->dy  * 1e6;
  sey = p->sey * 1e6;
  sex = p->sex * 1e6;
  double xSI = x*1e-6;
  sig_l = las->SigmaX(xSI - p->dx) * 1e6;
  sig_s = std::sqrt( (std::pow(sey,2) + std::pow(sig_l,2)) );

  //p1 = 1.0/(2*LWS::pi*sex);
  p2 = 1.0/sig_s;
  p3 = -0.5*pow((x/sex),2);
  p4 = -0.5*pow((dy/sig_s),2);

  res = p2*exp(p3+p4);
  return res;
}

double LWS::PhotonNormalisation(const Laser* laser,
				const double numberOfElectrons,
				const double electronEnergy)
{
  double comptonCrossSection = LWS::ComptonCrossSection(laser, electronEnergy);
  double laserPeakPower      = laser->PeakPower();
  double laserWavelength     = laser->Wavelength();
  double denominator         = LWS::h * LWS::c_light_squared;
  double nominator           = laserPeakPower * numberOfElectrons * laserWavelength * comptonCrossSection;
  
  return nominator / denominator;
}

double LWS::PhotonNormalisationIons(const Laser* laser,
                                const double numberOfIons,
                                const double ionEnergy)
{
    double photodetachmentCrossSection = LWS::PhotodetachmentCrossSection(laser, ionEnergy);
    double laserPeakPower      = laser->PeakPower();
    double laserWavelength     = laser->Wavelength();
    double denominator         = LWS::h * LWS::c_light_squared;
    double nominator           = laserPeakPower * numberOfIons * laserWavelength * photodetachmentCrossSection;
    return nominator / denominator;
}

double LWS::ComptonPhotonsGaussian(const Laser* laser,
				   const double numberOfElectrons,
				   const double electronEnergy,
				   const double sigma_ey,
				   const double dy)
{
  double overlapIntegral = LWS::OIGaussianSI(laser, sigma_ey, dy);
  
  double normalisation  = LWS::PhotonNormalisation(laser, numberOfElectrons, electronEnergy);
  double comptonPhotons = normalisation * overlapIntegral;
  
  return comptonPhotons;
}

double LWS::PhotodetachmentPhotonsGaussian(const Laser* laser,
                                   const double numberOfIons,
                                   const double ionEnergy,
                                   const double sigma_ey,
                                   const double dy)
{
    double overlapIntegral = LWS::OIGaussian(laser, sigma_ey, dy);
    double normalisation  = LWS::PhotonNormalisationIons(laser, numberOfIons, ionEnergy);
    double neutrals = normalisation * overlapIntegral;
    return neutrals;
}

double LWS::ComptonPhotons(const Laser*  laser,
			   const double  numberOfElectrons,
			   const double  electronEnergy,
			   const double& sigma_ex,
			   const double& sigma_ey,
			   const double& dx,
			   const double& dy,
			   gsl_integration_workspace* w,
			   double epsabs,
			   double epsrel)
{
  double overlapIntegral = LWS::OI(laser, sigma_ex, sigma_ey, dx, dy, w, epsabs, epsrel);
  double p2              = 1./(LWS::twopi * sigma_ex);
  double normalisation   = LWS::PhotonNormalisation(laser, numberOfElectrons, electronEnergy);
  double comptonPhotons  = normalisation * p2 * overlapIntegral;
  
  return comptonPhotons;
}

double LWS::PDHydrogens(const Laser*  laser,
                           const double  numberOfIons,
                           const double  ionEnergy,
                           const double& sigma_ex,
                           const double& sigma_ey,
                           const double& dx,
                           const double& dy,
                           gsl_integration_workspace* w,
                           double epsabs,
                           double epsrel)
{
    double overlapIntegral = LWS::OI(laser, sigma_ex, sigma_ey, dx, dy, w, epsabs, epsrel);
    double p2              = 1./(LWS::twopi * sigma_ex);
    double normalisation   = LWS::PhotonNormalisationIons(laser, numberOfIons, ionEnergy);
    double pdHydrogens  = normalisation * p2 * overlapIntegral;
    return pdHydrogens;
}

double LWS::NPhotonsBlair(const Laser* l,
                          double nElectrons,
                          double electronEnergy,
                          double electronSigma)
{
  double comptonXS = LWS::ComptonCrossSection(l, electronEnergy);
  double sigmaS = std::sqrt(std::pow(l->SigmaY(0), 2) + std::pow(electronSigma, 2));
  double result = nElectrons * l->PeakPower() * comptonXS * l->Wavelength() / (LWS::c_light_squared * LWS::h);
  double factor = 1.0 / (std::sqrt(LWS::twopi) * sigmaS);
  result *= factor;
  return result;
}