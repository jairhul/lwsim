#include "Laser.hh"
#include "Laserwire.hh"
#include "LaserwireScan.hh"
#include "Point.hh"

#include <gsl/gsl_integration.h>

#include <iostream>
#include <iterator>
#include <vector>

std::vector<double> LWS::GenerateXData(const double& start,
				       const double& stop,
				       const int&    nSteps)
{
  std::vector<double> result;
  double stepSize = (stop - start) / (double)nSteps;
  for (int i = 0; i < nSteps; i++)
    {result.push_back(start + (double)i * stepSize);}
  return result;
}

std::vector<double> LWS::GenerateNLXData(const double& centre,
					 const int&    nSteps,
					 const double& sigma_y,
					 const double  minimumStepSize)
{
  int    p              = 3;
  double fullRange      = sigma_y * 150;
  double halfRange      = 0.5 * fullRange;
  double linearStepSize = fullRange / (double)nSteps;

  // generate uniformly spaced points
  std::vector<double> y;
  for (double v = 0; v < halfRange+1e-15; v += linearStepSize)
    {y.push_back(v);}

  // take the cube of those points
  std::vector<double> yp; // y to a (p)ower
  for (auto const v : y)
    {yp.push_back(std::pow(v,p));}

  // maximum value is last (as 0 -> +ve)
  double maxVal = yp.back();

  // normalise cubic data and scale to half range
  std::vector<double> yn; // y (n)ormalised
  for (auto const y : yp)
    {yn.push_back( (y / maxVal) * halfRange);}

  // apply minimum step size on half side data
  for (int i = 1; i < yn.size(); i++)
    {
      if ((yn[i] - yn[i-1]) < minimumStepSize) // difference can be -ve
	{yn[i] = yn[i-1] + minimumStepSize;}
      else if (yn[i] <= yn[i-1]) // just in case
	{yn[i] = yn[i-1] + minimumStepSize;}
      else // it's fine, skip over
	{continue;}
    }

  // append first a negative reversed version and then the original data
  // to create full cubic displacement about centre.
  std::vector<double> result;
  std::vector<double>::iterator it = yn.end();
  it--; // make iterator point to last item, not beyond vector
  for (; it != yn.begin(); it--) // never reaches 0th item, so doesn't include 0
    {result.push_back(-1*(*it));} 
  for (auto const& val : yn) // includes 0
    {result.push_back(val);}

  // offset whole scan by centre;
  for (auto& v : result)
    {v += centre;}
  
  return result;
}

std::vector<double> LWS::VerticalScanOI(const Laser*               laser,
					const double&              sigma_ex,
					const double&              sigma_ey,
					const std::vector<double>& dy,
					const double               dx)
{
  gsl_integration_workspace* w = gsl_integration_workspace_alloc(100000);

  std::vector<double> result;
  double tempResult = 0;
  for (const auto& y : dy)
    {
      tempResult = LWS::OI(laser, sigma_ex, sigma_ey, dx, y, w);
      result.push_back(tempResult);
    }

  gsl_integration_workspace_free(w);
  
  return result;
}

std::vector<double> LWS::HorizontalScanOI(const Laser*               laser,
					  const double&              sigma_ex,
					  const double&              sigma_ey,
					  const std::vector<double>& dx,
					  const double               dy)
{
  gsl_integration_workspace* w = gsl_integration_workspace_alloc(100000);

  std::vector<double> result;
  for (const auto& x : dx)
    {result.push_back(LWS::OI(laser, sigma_ex, sigma_ey, x, dy, w));}

  gsl_integration_workspace_free(w);
  
  return result;
}

std::vector<double> LWS::VerticalScan(const Laser*               laser,
				      const double               numberOfElectrons,
				      const double               electronEnergy,
				      const double&              sigma_ex,
				      const double&              sigma_ey,
				      const std::vector<double>& dy,
				      const double               dx)
{
  gsl_integration_workspace* w = gsl_integration_workspace_alloc(100000);

  std::vector<double> result;
  double tempResult = 0;
  for (const auto& y : dy)
    {
      tempResult = LWS::ComptonPhotons(laser, numberOfElectrons, electronEnergy,
				       sigma_ex, sigma_ey, dx, y, w);
      result.push_back(tempResult);
    }

  gsl_integration_workspace_free(w);

  return result;
}

std::vector<double> LWS::VerticalScanPD(const Laser*               laser,
                                      const double               numberOfIons,
                                      const double               ionEnergy,
                                      const double&              sigma_ex,
                                      const double&              sigma_ey,
                                      const std::vector<double>& dy,
                                      const double               dx)
{
    gsl_integration_workspace* w = gsl_integration_workspace_alloc(100000);

    std::vector<double> result;
    double tempResult = 0;
    for (const auto& y : dy)
    {
        tempResult = LWS::PDHydrogens(laser, numberOfIons, ionEnergy,
                                         sigma_ex, sigma_ey, dx, y, w);
        result.push_back(tempResult);
    }

    gsl_integration_workspace_free(w);

    return result;
}

std::vector<double> LWS::HorizontalScan(const Laser*               laser,
					const double               numberOfElectrons,
					const double               electronEnergy,
					const double&              sigma_ex,
					const double&              sigma_ey,
					const std::vector<double>& dx,
					const double               dy)
{
  gsl_integration_workspace* w = gsl_integration_workspace_alloc(100000);

  std::vector<double> result;
  for (const auto& x : dx)
    {result.push_back(LWS::ComptonPhotons(laser, numberOfElectrons, electronEnergy,
					  sigma_ex, sigma_ey, x, dy, w));}

  gsl_integration_workspace_free(w);

  return result;
}

std::vector<double> LWS::VerticalScanGauss(const Laser*               laser,
					   const double               numberOfElectrons,
					   const double               electronEnergy,
					   const double               sigma_ex,
					   const double               sigma_ey,
					   const std::vector<double>& dy)
{
  std::vector<double> result;
  for (const auto& y : dy)
    {result.push_back(LWS::ComptonPhotonsGaussian(laser, numberOfElectrons, electronEnergy,
						  sigma_ey, y));}

  return result;
}


std::vector<double> LWS::VerticalScanGaussIons(const Laser*               laser,
                                           const double               numberOfIons,
                                           const double               ionEnergy,
                                           const double               sigma_ex,
                                           const double               sigma_ey,
                                           const std::vector<double>& dy)
{
    std::vector<double> result;
    for (const auto& y : dy)
    {result.push_back(LWS::PhotodetachmentPhotonsGaussian(laser, numberOfIons, ionEnergy,
                                                  sigma_ey, y));}

    return result;
}


std::vector<double> LWS::VerticalScanOIGauss(const Laser*               laser,
					     const double               sigma_ey,
					     const std::vector<double>& dy)
{
  std::vector<double> result;
  for (const auto& y : dy)
    {result.push_back(LWS::OIGaussian(laser, sigma_ey, y));}

  return result;
}

std::vector<LWS::point> LWS::TwoDScanOI(const Laser*               laser,
					const double&              sigma_ex,
					const double&              sigma_ey,
					const std::vector<double>& xPositions,
					const std::vector<double>& yPositions)
{
  gsl_integration_workspace* w = gsl_integration_workspace_alloc(100000);
  
  std::vector<LWS::point> result;
  double tempResult = 0;
  for (const auto& y : yPositions)
    {
      for (const auto& x : xPositions)
	{
	  tempResult = LWS::OI(laser, sigma_ex, sigma_ey, x, y, w);
	  LWS::point pResult = {x,y,tempResult};
	  result.push_back(pResult);
	}
    }

  gsl_integration_workspace_free(w);
  
  return result;
}

std::vector<LWS::point> LWS::TwoDScan(const Laser*               laser,
				      const double               numberOfElectrons,
				      const double               electronEnergy,
				      const double&              sigma_ex,
				      const double&              sigma_ey,
				      const std::vector<double>& xPositions,
				      const std::vector<double>& yPositions)
{
  std::vector<LWS::point> result = LWS::TwoDScanOI(laser, sigma_ex, sigma_ey,
						   xPositions, yPositions);

  double normalisation = 0;
  for (auto& point : result)
    {
      normalisation = LWS::PhotonNormalisation(laser, numberOfElectrons, electronEnergy);
      point.value *= normalisation;
    }

  return result;
}
