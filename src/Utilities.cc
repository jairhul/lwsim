#include "Constants.hh"
#include "Utilities.hh"

#include <cmath>
#include <limits>

bool LWS::IsFinite(const double& number)
{
  return std::abs(number) > std::numeric_limits<double>::epsilon();
}

double LWS::RayleighRange(const double& wavelength,
			  const double& w0,
			  const double  m2)
{
  return LWS::pi * std::pow(w0, 2) / (m2 * wavelength);
}
