#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "LaserwireScan.hh"

#include <vector>


int main()
{
  double sigma_ex_narrow = 1e-6;
  double sigma_ex_wide   = 100e-6;
  double sigma_ey = 1.2e-6;

  LWS::Laser* l = new LWS::LaserGaussian(532e-9, 1.8e-6, 1.2);

  std::vector<double> yPositions = LWS::GenerateXData(-10e-6, 10e-6, 50);

  std::vector<double> gScan = LWS::VerticalScanOIGauss(l, sigma_ey, yPositions);
  LWS::WriteScanToFile("vertCompareGauss.txt", yPositions, "Y", gScan, "OverlapIntegralGauss");

  std::vector<double> oiScan = LWS::VerticalScanOI(l, sigma_ex_narrow, sigma_ey, yPositions,  0);
  LWS::WriteScanToFile("vertCompareEXNarrowOI.txt", yPositions, "Y", oiScan, "OverlapIntegral");

  std::vector<double> oiScan2 = LWS::VerticalScanOI(l, sigma_ex_wide, sigma_ey, yPositions,  0);
  LWS::WriteScanToFile("vertCompareEXWideOI.txt", yPositions, "Y", oiScan2, "OverlapIntegral");
  
  delete l;

  return 0;
}
