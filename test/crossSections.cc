#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "Laserwire.hh"

#include <iostream>
#include <vector>

int main()
{
  // construct laser focus instances with 2.3um W0 and M2 of 1.2 - typical of
  // green laserwire
  const double w0 = 2.3e-6;
  const double m2 = 1.2;
  LWS::Laser* lBlue  = new LWS::LaserGaussian(266e-9,  w0, m2); // quadrupuled Nd:YAG
  LWS::Laser* lGreen = new LWS::LaserGaussian(532e-9,  w0, m2); // doubled Nd:YAG
  LWS::Laser* lIR    = new LWS::LaserGaussian(1064e-9, w0, m2); // Nd:YAG

  // electron energy / gamma
  double energy = 1.3; // GeV - ATF2 energy
  double gamma = LWS::ElectronLorentzGammaFromEnergy(energy);
  std::cout << "Relativistic gamma for e- at " << energy << " GeV: " << gamma << std::endl;

  // compton cross-section
  double atf2ComptonCS = LWS::ComptonCrossSection(lGreen, 1.3);
  std::cout << "Compton cross-section for " << lGreen->Wavelength() / 1e-9
	    << " nm on an e- at " << energy << " GeV: "
	    << atf2ComptonCS << " m^2" << std::endl;

  /// Write out tables of cross-section versus electron energy for 3 wavelengths.
  std::vector<double> blueCS;
  std::vector<double> greenCS;
  std::vector<double> irCS;
  std::vector<double> energies;
  for (double en = 1; en < 501; en += 1.)
    {energies.push_back(en);}

  std::cout << "Example Compton to Thomson ratios for different wavelengths for e- at "
	    << energy << " GeV" << std::endl;
  std::cout << "266 nm:  " << LWS::ComptonThomsonRatio(lBlue,  energy) << std::endl;
  std::cout << "532 nm:  " << LWS::ComptonThomsonRatio(lGreen, energy) << std::endl;
  std::cout << "1064 nm: " << LWS::ComptonThomsonRatio(lIR,    energy) << std::endl;

  for (const auto& en : energies)
    {
      blueCS.push_back(LWS::ComptonThomsonRatio(lBlue, en));
      greenCS.push_back(LWS::ComptonThomsonRatio(lGreen, en));
      irCS.push_back(LWS::ComptonThomsonRatio(lIR, en));
    }

  LWS::WriteScanToFile("266nmCtoTRatio.txt",  energies, "Energy", blueCS,  "CtoTRatio");
  LWS::WriteScanToFile("532nmCtoTRatio.txt",  energies, "Energy", greenCS, "CtoTRatio");
  LWS::WriteScanToFile("1064nmCtoTRatio.txt", energies, "Energy", irCS,    "CtoTRatio");
  
  // photon normalisation
  double normalisation = LWS::PhotonNormalisation(lGreen, 1e10, 250);
  std::cout << "Photon normalisation for overlap integral: " << normalisation << std::endl;
  
  delete lBlue;
  delete lGreen;
  delete lIR;
  
  return 0;
}
