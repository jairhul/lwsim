#include "Fit.hh"
#include "FitParameters.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "LaserwireScan.hh"

#include <iostream>
#include <random>
#include <vector>

int main()
{
  double sigma_ex = 100e-6; // m
  double sigma_ey = 1e-6;   // m
  double noiseFraction = 0.02;  // %
  
  LWS::Laser* l = new LWS::LaserGaussian(532e-9, 2.3e-6, 1.2);

  //std::vector<double> yPositions  = LWS::GenerateXData(-80e-6, 80e-6, 50);
  std::vector<double> yPositions2 = LWS::GenerateNLXData(0, 50, sigma_ey);
  
  std::vector<double> scan  = LWS::VerticalScanOI(l, sigma_ex, sigma_ey, yPositions2,  0);

  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);
  auto dice = std::bind(distribution, generator);
  std::vector<double> scanWNoise;
  std::vector<double> errors;
  for (auto v : scan)
    {
      scanWNoise.push_back(v + noiseFraction * dice() * v);
      errors.push_back(v*noiseFraction);
    }

  LWS::FitParameters fit =  LWS::FitVerticalScanEmpirical(l,
							  sigma_ex,
							  yPositions2,
							  scan,
							  errors,
							  true);

  std::cout << fit << std::endl;

  delete l;

  return 0;
}
