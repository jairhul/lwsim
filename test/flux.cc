#include "Constants.hh"
#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "Laserwire.hh"
#include "LaserwireScan.hh"

#include <cmath>
#include <iostream>
#include <map>
#include <vector>

int main()
{
  // wavelength, w0, m2, sigmaT, pulseEnergy
  LWS::LaserGaussian* laserATF2 = new LWS::LaserGaussian(532e-9, 2e-6, 1.2, 70e-12, 0.125); // 125mJ
  std::cout << "Laser peak power " << laserATF2->PeakPower()/1e6 << " MW" << std::endl;
  std::cout << "Laser Rayleigh Range " << laserATF2->RayleighRangeX()*1e6 << " um" << std::endl;
  
  // e beam information
  double sigmaTElectrons = 30e-12;
  double sigma_ex = 100e-6;
  double sigma_ey = 1e-6;
  double eEnergyInGeV = 1.3;
  double NePerBunch = 1e9; // e-
  
  std::cout << "e- beam: " << eEnergyInGeV << " GeV, sigma_x,y = (" << sigma_ex*1e6 << ", "
            << sigma_ey*1e6 << ") um" << std::endl;
  std::cout << "sigma_t (e-): " << sigmaTElectrons*1e12 << " ps, Ne / bunch: " << NePerBunch << std::endl;

  double laserOffsetX = 0;
  double laserOffsetY = 0;

  // cross-section information
  double tcRatio = LWS::ComptonThomsonRatio(laserATF2, eEnergyInGeV);
  std::cout << "Thomson XS / Compton XS " << tcRatio << std::endl;
  double comptonXS = LWS::ComptonCrossSection(laserATF2, eEnergyInGeV);
  std::cout << "Compton XS " << comptonXS << " m" << std::endl;
  
  // compton spectra information
  double maxGammaInGeV = LWS::MaximumComptonPhotonEnergy(laserATF2, eEnergyInGeV);
  std::cout << "Maximum Compton photon energy " << maxGammaInGeV*1e3 << " MeV" << std::endl;
  
  // compton flux information
  double resultGauss = LWS::ComptonPhotonsGaussian(laserATF2, NePerBunch, eEnergyInGeV, sigma_ey, laserOffsetY);
  double resultOI    = LWS::ComptonPhotons(laserATF2, NePerBunch, eEnergyInGeV, sigma_ex, sigma_ey, laserOffsetX, laserOffsetY);
  std::cout << "Nc (gauss approximation) = " << resultGauss << std::endl;
  std::cout << "Nc (full overlap integral) = " << resultOI << std::endl;
  
  double resultOISymmetricEBeam = LWS::ComptonPhotons(laserATF2, NePerBunch, eEnergyInGeV, laserATF2->RayleighRangeX(), sigma_ey, laserOffsetX, laserOffsetY);
  std::cout << "Nc (full overlap integral) for " << laserATF2->RayleighRangeX()*1e6 << " x 1um e- beam = " << resultOISymmetricEBeam << std::endl;
  
  delete laserATF2;
  
  return 0;
}
