#include "Laser.hh"
#include "LaserGaussian.hh"
#include "Laserwire.hh"

#include <iostream>

int main()
{
  LWS::Laser* lGreen = new LWS::LaserGaussian(532e-9,  2.3e-6, 1.2); // doubled Nd:YAG

  double result = LWS::OI(lGreen, 100e-6, 1e-6, 0, 0);

  std::cout << "Result: " << result << std::endl;
  
  return 0;
}
