#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "LaserwireScan.hh"
#include "Point.hh"

#include <iostream>
#include <vector>

int main()
{
  double sigma_ex       = 10e-6;  // m
  double sigma_ey       = 1e-6;   // m
  double nElectrons     = 2e10;
  double electronEnergy = 250;    // GeV

  // Green laser with w0 1.8 um, M2 1.1, sigma_t = 10ps, 300mJ pulse energy.
  LWS::Laser* l = new LWS::LaserGaussian(532e-9, 1.8e-6, 1.1, 10e-12, 3e-1);

  std::vector<double> xPositions = LWS::GenerateXData(-0.7e-3, 0.7e-3, 200);
  std::vector<double> yPositions = LWS::GenerateXData( -30e-6,  30e-6, 200);

  std::cout << "x size " << xPositions.size() << std::endl;
  std::cout << "y size " << yPositions.size() << std::endl;
  
  std::vector<LWS::point> result = LWS::TwoDScan(l, nElectrons, electronEnergy,
						 sigma_ex, sigma_ey,
						 xPositions, yPositions);

  LWS::WriteScan2DToFile("photons2D.txt", result);

  delete l;

  return 0;
}
