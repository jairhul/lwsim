import matplotlib.pyplot as plt
import numpy as np


def LoadScan(filename):
    f = open(filename)
    lines = f.readlines()
    f.close()

    columns = lines[0].strip().split()

    data = []
    for line in lines[1:]:
        data.append(line.strip().split())

    data = np.array(data, dtype=float)

    x = data[:,0]
    y = data[:,1]

    d = {columns[0]:x, columns[1]:y, 'x':x, 'y':y,
         'xlabel':columns[0], 'ylabel':columns[1]}
    return d
    
def PlotScan(filename):
    d = LoadScan(filename)

    plt.figure()
    plt.plot(d['x'], d['y'], '.')
    plt.xlabel(d['xlabel'])
    plt.ylabel(d['ylabel'])
