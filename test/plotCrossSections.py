import plot

import matplotlib.pyplot as plt

def main():
    blue  = plot.LoadScan("266nmCtoTRatio.txt")
    green = plot.LoadScan("532nmCtoTRatio.txt")
    ir    = plot.LoadScan("1064nmCtoTRatio.txt")

    plt.figure()
    plt.plot(blue['x'],  blue['y'],  'b', label='266 nm')
    plt.plot(green['x'], green['y'], 'g', label='532 nm')
    plt.plot(ir['x'],    ir['y'],    'r', label='1064 nm')
    plt.legend()
    plt.xlabel('Energy (GeV)')
    plt.ylabel('$\sigma_{T} / \sigma_{C}$')
    plt.ylim(0,1.05)
    plt.tight_layout()
