import plotter

import matplotlib.pyplot as plt


def main():
    
    sigmas = [0,1,2,3,5,10,30]

    plt.figure()
    for s in sigmas:
        d = plot.LoadScan("horizontalOIScan"+str(s)+".txt")
        plt.plot(d['x']*1e3, d['y'], label='dy = '+str(s)+'$\sigma_{y}$')
    
    plt.xlabel('dX (mm)')
    plt.ylabel('Overlap Integral')
    plt.legend()
    plt.axhline(0, alpha=0.2, color='grey')
    plt.tight_layout()
