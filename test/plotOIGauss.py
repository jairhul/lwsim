import plot

import matplotlib.pyplot as plt


def main():
    g  = plot.LoadScan("vertCompareGauss.txt")
    o1 = plot.LoadScan("vertCompareEXNarrowOI.txt")
    o2 = plot.LoadScan("vertCompareEXWideOI.txt")

    plt.figure()
    plt.plot(g['x']*1e6, g['y'], '.',
             label='Gaussian (both cases)')
    plt.plot(o1['x']*1e6, o1['y'], 'x',
             label='Numerical Integral ($\sigma_{ex} = 1 \mu$m)',
             alpha=0.8)
    plt.plot(o2['x']*1e6, o2['y'], '^', ms=2,
             label='Numerical Integral ($\sigma_{ex} = 100 \mu$m)')

    plt.xlabel('Y ($\mu$m)')
    plt.ylabel('Overlap Integral')
    plt.legend(fontsize='x-small')
    plt.tight_layout()
