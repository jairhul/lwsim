import numpy as np
import matplotlib.pyplot as plt

def main():
    

    d = np.loadtxt("photons2D.txt")

    dim = int(np.sqrt(np.shape(d)[0]))

    x2d = d[:,0].reshape(dim,dim) * 1e3
    y2d = d[:,1].reshape(dim,dim) * 1e6
    z2d = d[:,2].reshape(dim,dim)

    plt.figure()

    plt.contourf(x2d,y2d,z2d,20)
    plt.colorbar()
    plt.xlabel('X Position (mm)')
    plt.ylabel('Y Position ($\mu$m)')
    plt.tight_layout()
