import plot

import matplotlib.pyplot as plt

def main():

    plt.figure()

    linear    = plot.LoadScan("verticalOIScan.txt")
    nonlinear = plot.LoadScan("verticalOIScan2.txt")

    plt.figure()
    plt.plot(linear['x']*1e6, linear['y'], 'x', label='Linear')
    plt.plot(nonlinear['x']*1e6, nonlinear['y'], '.', label='Non-linear')
    plt.xlabel('X ($\mu$m)')
    plt.ylabel('Overlap Integral')
    plt.legend()
    plt.tight_layout()
