#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "LaserwireScan.hh"

#include <vector>

int main()
{
  double sigma_ex = 100e-6; // m
  double sigma_ey = 1e-6;   // m
  
  LWS::Laser* l = new LWS::LaserGaussian(532e-9, 2.3e-6, 1.2);

  std::vector<double> yPositions  = LWS::GenerateXData(-80e-6, 80e-6, 50);
  std::vector<double> yPositions2 = LWS::GenerateNLXData(0, 50, sigma_ey);
  
  std::vector<double> scan  = LWS::VerticalScanOI(l, sigma_ex, sigma_ey, yPositions,  0);
  LWS::WriteScanToFile("verticalOIScan.txt", yPositions, "Y", scan, "OverlapIntegral");

  std::vector<double> scan2 = LWS::VerticalScanOI(l, sigma_ex, sigma_ey, yPositions2, 0);
  LWS::WriteScanToFile("verticalOIScan2.txt", yPositions2, "Y", scan2, "OverlapIntegral");

  delete l;

  return 0;
}
