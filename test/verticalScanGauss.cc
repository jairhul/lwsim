#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "Laserwire.hh"
#include "LaserwireScan.hh"

#include <vector>

int main()
{

  double sigma_ey = 1.2e-6;

  LWS::Laser* l = new LWS::LaserGaussian(532e-9, 2.3e-6, 1.2);

  std::vector<double> yPositions = LWS::GenerateXData(-10e-6, 10e-6, 50);

  std::vector<double> scan = LWS::VerticalScanOIGauss(l, sigma_ey, yPositions);
  LWS::WriteScanToFile("verticalOIScanGauss.txt", yPositions, "Y", scan, "OverlapIntegralGauss");
  
  delete l;

  return 0;
}
