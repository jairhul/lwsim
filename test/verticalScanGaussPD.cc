#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "Laserwire.hh"
#include "LaserwireScan.hh"

#include <vector>

int main()
{

  //double sigma_ey = 1.2e-6;

//  LWS::Laser* l = new LWS::LaserGaussian(532e-9, 2.3e-6, 1.2);

//  std::vector<double> yPositions = LWS::GenerateXData(-10e-6, 10e-6, 50);

//  std::vector<double> scan = LWS::VerticalScanOIGauss(l, sigma_ey, yPositions);
 // LWS::WriteScanToFile("verticalOIScanGauss.txt", yPositions, "Y", scan, "OverlapIntegralGauss");

    double sigma_ex       = 2.03e-3;  // m
    double sigma_ey       = 7.2e-3;   // m
    double nIons     = 1.16e9;
    double ionEnergy = 0.93929401082+0.003;    // GeV

    // Green laser with w0 1.8 um, M2 1.1.
    LWS::Laser* l = new LWS::LaserGaussian(1064e-9, 55.0e-6, 1.1,106.0e-9,67.4e-6);
    std::vector<double> yPositions = LWS::GenerateXData(-20e-3, 20e-3, 50);

    std::vector<double> scan  = LWS::VerticalScanGaussIons(l, nIons, ionEnergy,
                                                  sigma_ex, sigma_ey, yPositions);
    LWS::WriteScanToFile("FETSLikeGaussVertical.txt", yPositions, "Y", scan, "NH0");



    delete l;

  return 0;
}
