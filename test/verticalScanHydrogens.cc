#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "LaserwireScan.hh"

#include <vector>

int main()
{
  double sigma_ex       = 2.03e-3;  // m
  double sigma_ey       = 7.2e-3;   // m
  double nIons     = 1.16e9;
  double ionEnergy = 0.93929401082+0.003;    // GeV

  // Green laser with w0 1.8 um, M2 1.1.
  LWS::Laser* l = new LWS::LaserGaussian(1064e-9, 55.0e-6, 1.2,50.0e-12,67.5e-6);
  
  std::vector<double> yPositions;

  for (int i = -25; i<26; ++i ) { yPositions.push_back(i*1.0e-3); }

  std::vector<double> scan  = LWS::VerticalScanPD(l, nIons, ionEnergy,
						sigma_ex, sigma_ey, yPositions,  0);
  LWS::WriteScanToFile("FETSLikeVerticalScan.txt", yPositions, "Y", scan, "NH0");


  delete l;

  return 0;
}
