#include "Fifo.hh"
#include "Laser.hh"
#include "LaserGaussian.hh"
#include "LaserwireScan.hh"

#include <vector>

int main()
{
  double sigma_ex       = 50e-6;  // m
  double sigma_ey       = 0.8e-6;   // m
  double nElectrons     = 2e10;
  double electronEnergy = 250;    // GeV

  // Green laser with w0 1.8 um, M2 1.1.
  LWS::Laser* l = new LWS::LaserGaussian(532e-9, 1.8e-6, 1.1);
  
  std::vector<double> yPositions = LWS::GenerateNLXData(0, 50, sigma_ey);
  
  std::vector<double> scan  = LWS::VerticalScan(l, nElectrons, electronEnergy,
						sigma_ex, sigma_ey, yPositions,  0);
  LWS::WriteScanToFile("verticalScanPhotons.txt", yPositions, "Y", scan, "NPhotons");


  delete l;

  return 0;
}
